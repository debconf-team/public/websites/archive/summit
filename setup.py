import os
from setuptools import setup


# Code below taken from Django
def fullsplit(path, result=None):
    """
    Split a pathname into components (the opposite of os.path.join) in a
    platform-neutral way.
    """
    if result is None:
        result = []
    head, tail = os.path.split(path)
    if head == '':
        return [tail] + result
    if head == path:
        return result
    return fullsplit(head, [tail] + result)

data_files = []
packages = []
for dirpath, dirnames, filenames in os.walk('summit'):
    # Ignore dirnames that start with '.'
    for i, dirname in enumerate(dirnames):
        if dirname.startswith('.'):
            del dirnames[i]
    if '__init__.py' in filenames:
        packages.append('.'.join(fullsplit(dirpath)))
    elif filenames:
        data_files.append([os.path.join('share', dirpath),
                          [os.path.join(dirpath, f) for f in filenames]])

print data_files
print packages

setup(
    name = "summit",
    version = "1.0.0",
    author = "Canonical Ltd",
    license = "AGPLv3",
    packages = packages,
    zip_safe = False,
    install_requires = [
        "Django==1.4.5",
        "psycopg2>=2.0.9",
        "pytz",
        "BeautifulSoup",
        "python-openid",
        "django-openid-auth>=0.5",
        "South>=0.6",
    ],
    data_files = data_files,
)

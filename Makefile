pip=env/bin/pip
py=env/bin/python
bzr=/usr/bin/bzr
django_manage=cd ./summit/ && ../$(py) manage.py
branch=$(bzr) branch
projectroot=$(shell pwd |sed -e 's/\//\\\//g')

init: env db
	$(django_manage) init-summit

env:
	virtualenv --clear --no-site-packages --python=/usr/bin/python2.7 env
	$(pip) install -r requirements.txt
	cp summit/local_settings.py.sample summit/local_settings.py

summit/bzr_apps: env
	$(branch) lp:ubuntu-django-foundations/bzr-apps summit/bzr_apps
	$(django_manage) pullapps

apps: env summit/bzr_apps

summit/summit.db: env apps
	$(django_manage) syncdb --all
	$(django_manage) migrate --fake

db: summit/summit.db

lp: env apps db
	$(django_manage) update

run: env apps db
	$(django_manage) runserver

test: env apps
	$(django_manage) test schedule sponsor

clean:
	rm -rf env/
	rm -rf lp_data/
	rm -rf data/
	rm -rf summit/bzr_apps/
	rm -f summit/summit.db

depends:
	apt-get install python2.7 python2.7-dev python-virtualenv libjs-jquery libjs-jquery-ui iso-codes python-django python-django-auth-openid python-django-south python-beautifulsoup python-psycopg2 python-openid python-tz python-simplejson python-markdown openssh-client bzr python-django-reversion


.DEFAULT: env apps
.PHONY: run clean check live

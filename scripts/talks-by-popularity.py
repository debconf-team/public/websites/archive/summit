#!/bin/sh
sh=''''
    dir=$(readlink -f $(dirname $0)/../env/bin)
    if [ -n "$dir" ]; then
        export PATH=$dir:$PATH
    fi
    script=$(readlink -f $0)
    cd $(dirname $0)/../summit
    exec python "$script" "$@"
' '''

import sys, os
reload(sys)
sys.setdefaultencoding('utf-8')

import datetime

extra_paths = [
    os.path.abspath('..'),
    os.path.join(os.path.abspath('..'), 'summit')
]
for path in extra_paths:
    if path not in sys.path:
       sys.path.append(path)

os.environ['DJANGO_SETTINGS_MODULE'] = 'debconf_settings'

from schedule.models import Meeting

meetings = Meeting.objects.filter(
                 summit__name='debconf14',
           )

for meeting in meetings:
    print meeting.title, meeting.id
person.badge_full

SELECT sm.title,sm.id,ss.start,ss.end,sum.title,sr.name AS room,COUNT(sp.id) FROM schedule_agenda sa LEFT JOIN schedule_meeting sm ON sa.meeting_id=sm.id LEFT JOIN schedule_slot ss ON sa.slot_id=ss.id LEFT JOIN schedule_summit sum ON ss.summit_id=sum.id LEFT JOIN schedule_room sr ON sa.room_id=sr.id LEFT JOIN schedule_participant sp ON sm.id=sp.meeting_id GROUP BY sm.title,sm.id,ss.start,ss.end,sum.title,sr.name ORDER BY ss.start,ss.end;


#! /usr/bin/python

import httplib2
import urllib
import simplejson

# Configure these before running
LP_COOKIE = 'lp=replace-with-yours'
LP_HOST = 'launchpad.net'
SPRINT_NAME = 'uds-1305'
SPRINT_START = '2013-05-14 14:00'
SPRINT_END = '2013-05-16 20:00'
# Leave the rest alone

client = httplib2.Http()
resp, data = client.request('http://summit.ubuntu.com/api/user/?attendee__summit__name=%s&attendee__from_launchpad=0' % SPRINT_NAME)

users = simplejson.loads(data)

register_url = 'https://%s/sprints/%s/+register' % (LP_HOST, SPRINT_NAME)
headers = {
            'Cookie': LP_COOKIE,
            'Referer': 'https://%s/sprints/%s/+register' % (LP_HOST, SPRINT_NAME),
          }

# Used to limit the http calls during testing
#users = [{'username': 'cjohnston'}]

for user in users:
    print 'Rgistering %s' % user['username']
    post_data = urllib.urlencode({
        'field.attendee': user['username'],
        'field.time_starts': SPRINT_START,
        'field.time_ends': SPRINT_END,
        'field.is_physical': 'no',
        'field.is_physical-empty-marker': 1,
        'field.actions.register': 'Register',
    })
    resp, data = client.request(register_url, method='POST', body=post_data, headers=headers)
    if resp['status'] != '200':
        print 'Failed to register user: %s' % user['username']
exit


#!/bin/sh
sh=''''
    dir=$(readlink -f $(dirname $0)/../env/bin)
    if [ -n "$dir" ]; then
        export PATH=$dir:$PATH
    fi
    script=$(readlink -f $0)
    cd $(dirname $0)/../summit
    exec python "$script" "$@"
' '''

import sys, os
reload(sys)
sys.setdefaultencoding('utf-8')

extra_paths = [
    os.path.abspath('..'),
    os.path.join(os.path.abspath('..'), 'summit')
]
for path in extra_paths:
    if path not in sys.path:
        sys.path.append(path)

os.environ['DJANGO_SETTINGS_MODULE'] = 'debconf_settings'

import datetime
import pytz

from summit.schedule.models import Summit, Slot

summit = 'debconf14'
summit = Summit.objects.get(name=summit)
summit_days = summit.days()

def start_end(date, begin, duration):
    begin = datetime.datetime.strptime(begin, '%H:%M').time()
    slottime = pytz.timezone(summit.timezone).localize(datetime.datetime.combine(date, begin))
    start_time = summit.delocalize(slottime)
    end_time = start_time + datetime.timedelta(minutes=duration)
    return start_time, end_time

# Meals
brunch_days = [summit_days[d] for d in (0,1,7,8)]
lunch_days = [summit_days[d] for d in (2,3,5,6)]
breakfast_days = [summit_days[d] for d in range(2,7)]
dinner_days = summit_days

meals = [ 
    { 'meal': 'brunch', 'dates': brunch_days, 'begin': '10:30', 'duration': 150 },
    { 'meal': 'lunch', 'dates': lunch_days, 'begin': '12:00', 'duration': 90 },
    { 'meal': 'breakfast', 'dates': breakfast_days, 'begin': '08:00', 'duration': 90, },
    { 'meal': 'dinner', 'dates': dinner_days, 'begin': '18:00', 'duration': 120, },
        ]

slot_type = 'lunch'
for meal in meals:
    meal_name = meal.get('meal')
    begin = meal.get('begin')
    duration = meal.get('duration')
    for date in meal.get('dates'): 
        start, end = start_end(date, begin, duration)
        slot, created = Slot.objects.get_or_create(summit=summit, start_utc=start, end_utc=end, type=slot_type)
        if created:
            print "Created meal slot: %s" % slot

# Talks and Breaks
opening_day = [summit_days[0]]
morning_days = [summit_days[d] for d in (2,3,5,6)]
afternoon_days = [summit_days[d] for d in (1,2,3,5,6,7)]
closing_day = [summit_days[8]]

talks_or_breaks = [
    { 'slot_type': 'plenary', 'dates': opening_day, 'beginnings': ['15:00','16:00','17:00'], 'duration': 45 },
    { 'slot_type': 'open', 'dates': morning_days, 'beginnings': ['10:00','11:00'], 'duration': 45 },
    { 'slot_type': 'open', 'dates': afternoon_days, 'beginnings': ['13:30','14:30','16:00','17:00'], 'duration': 45, },
    { 'slot_type': 'break', 'dates': afternoon_days, 'beginnings': ['15:15'], 'duration': 45, },
    { 'slot_type': 'plenary', 'dates': closing_day, 'beginnings': ['13:30','14:30','15:30'], 'duration': 45, },
        ]

for tob in talks_or_breaks:
    slot_type = tob.get('slot_type')
    duration = tob.get('duration')
    for date in tob.get('dates'): 
        for begin in tob.get('beginnings'):
            start, end = start_end(date, begin, duration)
            slot, created = Slot.objects.get_or_create(summit=summit, start_utc=start, end_utc=end, type=slot_type)
            if created:
                print "Created slot: %s" % slot
    

from django.shortcuts import render_to_response, get_object_or_404
from debconf_website.models import (
	UserProfile,
	Sponsorship,
	DebianRole,
	DebConfRole,
	RegistrationLevel,
        FoodAccommodation,
        TShirtSize,
        Diet,
        PromotionCode,
        Sex,
	user_is_dd
)
from summit.schedule.models import Summit
import datetime
from PIL import Image

from summit.schedule.forms import Registration
from summit.sponsor.forms import SponsorshipScoreForm

from django.db import models
from django.forms import HiddenInput, ValidationError, FileField, ModelForm, BaseForm, NullBooleanSelect
from django.forms.models import modelformset_factory
from django.utils.translation import ugettext_lazy as _
from django.utils.timezone import now
from django.template import RequestContext, Context, loader
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.core.exceptions import ObjectDoesNotExist
from summit.schedule.decorators import summit_required, summit_only_required

def _get_cleaner(form, field):
    def clean_field():
         return getattr(form.instance, field, None)
    return clean_field

class ReadOnlyFieldsFormMixin(BaseForm):
    def __init__(self, *args, **kwargs):
        super(ReadOnlyFieldsFormMixin, self).__init__(*args, **kwargs)
        if hasattr(self, "read_only_fields"):
            if self.instance and self.instance.pk:
                for field in self.read_only_fields:
                    self.fields[field].widget.attrs['disabled'] = 'disabled'
                    setattr(self, "clean_" + field, _get_cleaner(self, field))

class ReadOnlyFormMixin(BaseForm):
    def __init__(self, *args, **kwargs):
        super(ReadOnlyFormMixin, self).__init__(*args, **kwargs)
        if self.instance and self.instance.pk:
            for field in self.fields:
                self.fields[field].widget.attrs['disabled'] = 'disabled'
                setattr(self, "clean_" + field, _get_cleaner(self, field))


class SponsorshipForm(ModelForm):
    class Meta:
        model = Sponsorship
        fields = (
            'needs_accomodation', 'needs_debcamp', 'needs_travel', 'job',
            'use',
            'costs', 'needed', 'financial_need',
            'benefit', 'further_info',
        )

    def __init__(self, *args, **kargs):
        super(SponsorshipForm, self).__init__(*args, **kargs)
        self.fields['needs_accomodation'].label = _("I request food and accommodation sponsorship for DebConf")
        self.fields['needs_travel'].label = _("I request travel sponsorship")
        self.fields['further_info'].label = _("Comments to the Bursaries Team")

    def clean(self):
        errstr = []
        if (self.cleaned_data['needs_travel']
            or self.cleaned_data['needs_accomodation']
            or self.cleaned_data['needs_debcamp']) \
           and self.instance.userprofile.registration_level != RegistrationLevel(1):
            errstr.append(
                'Professional or Corporate registrants may not request sponsorship'
            )
        if self.cleaned_data['needs_travel'] \
           and (not self.cleaned_data.get('costs', 0)
	        or not self.cleaned_data.get('needed', 0)):
            errstr.append(
                'For sponsorship travel requests, you must provide total '
                'expected travel costs and how much you need provided for '
                'attendance.'
            )
        if errstr:
            raise ValidationError(errstr)
        return super(SponsorshipForm, self).clean()

class SponsorshipReadOnlyForm(SponsorshipForm, ReadOnlyFormMixin):
    def __init__(self, *args, **kwargs):
        super(SponsorshipReadOnlyForm, self).__init__(*args, **kwargs)

class UserProfileForm(Registration):
    class Meta:
        model = UserProfile
        # Horrible, but there's no other way to declare the ordering
        # since we don't want to modify Attendee
        # Also, registration.html section are hardcoded now too.
        fields = (
                  # General
		  'attend', 'reconfirm',
                  'badge_full', 'badge_nick',
                  'contact_email', 'subscribe',
                  # Conference
                  'start_utc', 'end_utc',
                  'role_debian', 'role_debconf', 'registration_level', 'donation',
                  'food_accommodation', 'sex',
                  # Preferences
                  'tshirt_size', 'diet',
                  'child_care', 'arrival_needs', 'public_image', 'notes',
                  # Contact
                  'telephone', 'address', 'city_state', 'postcode', 'country',
                  'emergency_note',
        )

    def __init__(self, *args, **kargs):
        super(UserProfileForm, self).__init__(*args, **kargs)
        instance = getattr(self, 'instance', None)
        if instance:
            summit = instance.summit

	self.fields['attend'].label = 'I want to attend %s' % summit.title
        if summit and (summit.state == "setup" or summit.state == "registration"):
            # FIXME! the next line should be removed, when css will have text in gray
            self.fields['reconfirm'].widget = HiddenInput()
            self.fields['reconfirm'].label = 'Please reconfirm in June'
	    self.fields['reconfirm'].widget.attrs['disabled'] = 'disabled'
        else:
            #self.fields['reconfirm'].widget = HiddenInput()
	    self.fields['reconfirm'].label = 'Reconfirm attendance'

        self.fields['start_utc'].label = 'Venue arrival date/time'
        self.fields['end_utc'].label = 'Venue departure date/time'
        self.fields['registration_level'].label = 'Register as'

	self.fields['subscribe'].help_text = """\
Please be advised that we reserve the right to e-mail you directly with important announcements
regarding your registration and participation. The conference announcements list is more
informational."""

        self.fields['role_debian'].queryset = DebianRole.objects.filter(visible_in_form=True)
        self.fields['role_debconf'].queryset = DebConfRole.objects.filter(visible_in_form=True)
        self.fields['tshirt_size'].queryset = TShirtSize.objects.filter(visible_in_form=True).order_by('order')
        self.fields['registration_level'].queryset = RegistrationLevel.objects.filter(visible_in_form=True)
        self.fields['food_accommodation'].queryset = FoodAccommodation.objects.filter(visible_in_form=True)
        self.fields['diet'].queryset = Diet.objects.filter(visible_in_form=True)

        with_sponsorship = False
        with_accommodation = False
        with_food = False
        try:
            if instance and instance.userprofile.food_accommodation.id == 2:
                with_accommodation = True
            elif instance and instance.userprofile.food_accommodation.id == 4:
                with_sponsorship = True
                with_accommodation = True
            if instance and summit.state == "registration" and instance.sponsorship.got_accommodation == "Y":
                with_sponsorship = True
                with_accommodation = True
            with_food = with_accommodation
            if instance and instance.userprofile.food_accommodation.id == 3:
                with_food = True

        except (UserProfile.DoesNotExist, Sponsorship.DoesNotExist) as e:
            pass
        q = self.fields['food_accommodation'].queryset = FoodAccommodation.objects.filter(visible_in_form=True)
        if not with_accommodation:
            q = q.exclude(pk=2)
        if not with_sponsorship:
            q = q.exclude(pk=4)
        if not with_food:
            q = q.exclude(pk=3)
        self.fields['food_accommodation'].queryset = q 

    def clean(self):
        errstr = []

        summit = self.instance.summit

        # only after reconfirmation phase: attend and reconfirm should be compatible
        # Note: reconfirm could be None
        if self.cleaned_data['attend'] and not self.cleaned_data['reconfirm']:
            errstr.append("If you want to attend, please reconfirm (3rd and 4th fields from the top)")
        elif self.cleaned_data['reconfirm'] and not self.cleaned_data['attend']:
            errstr.append("If you don't want to attend, please deselect reconfirmation (3rd and 4th fields from the top)")

        if self.cleaned_data['attend'] and self.cleaned_data['role_debconf'] not in (DebConfRole(2),DebConfRole(3)):
	    # not volunteers and organizers
            if self.cleaned_data['start_utc'] < summit.date_arrival:
                errstr.append(
                    'DebConf/DebCamp arrival day is %s.  Please choose an '
                    'arrival time after this date.' % summit.date_arrival
                )
            if self.cleaned_data['end_utc'] > summit.date_departure:
                errstr.append(
                    'All attendees must check out by %s' % summit.date_departure
                )
        elif self.cleaned_data['attend']:
	    # staff
            if self.cleaned_data['start_utc'] < summit.date_arrival_staff:
                errstr.append(
                    'Staff arrival day is %s.  Please choose an '
                    'arrival time after this date.' % summit.date_arrival_staff
                )
            if self.cleaned_data['end_utc'] > summit.date_departure_staff:
                errstr.append(
                    'Staffs must check out by %s' % summit.date_departure_staff
                )

        if self.cleaned_data['attend'] and self.cleaned_data['end_utc'] < self.cleaned_data['start_utc']:
            errstr.append(
                'Arrival must be earlier then departure'
            )
        if errstr:
            raise ValidationError(errstr)
        return super(UserProfileForm, self).clean()


    def clean_public_image(self):
        image = self.cleaned_data.get('public_image', None)
        if image:
            # Check file size before potential processing DoS in Image.open().
            if image.size > 24*1024:
                raise ValidationError("Image file too large (max 24k)")
            img = Image.open(image)
            w, h = img.size
            if w > 144 or h > 144:
                raise ValidationError("Image dimensions too large (max 144x144)")
        return image

    def clean_role_debian(self):
        instance = getattr(self, 'instance', None)
        if instance and user_is_dd(instance.user):
            return DebianRole(1)
        elif instance and not user_is_dd(instance.user) \
             and self.cleaned_data['role_debian'] == DebianRole(1):
            raise ValidationError(
                'Attempted to register as a DD.  If you are a DD, please log in via debian.org SSO.'
            )
        else:
            return self.cleaned_data['role_debian']

def test(request):
    return render_to_response('debconf_website_base.html', {'debconf_website_media':'/debconf-website/media'})

@login_required
@summit_required
def registration(request, summit, attendee):
    if summit == None or summit.state == "archived":
        return render_to_response('errormsg.html', {
		'summit': summit,
                'errormsg': "Registration for %s is closed" % summit.title},
	    RequestContext(request))
    elif summit.state == "setup" and (attendee == None or attendee.user not in summit.managers.all()):
        return render_to_response('errormsg.html', {
                'summit': summit,
                'errormsg': "Registration for %s is not yet open for public" % summit.title},
            RequestContext(request))

    user = request.user

    errors = []
    sponsform = None
    promocode = None
    promo = None

    try:
        profile = UserProfile.objects.get(user=user, summit=summit)
    except UserProfile.DoesNotExist:
        if request.GET.get('promo'):
           promocode = request.GET.get('promo')
           promo = get_object_or_404(PromotionCode, code=promocode)
           if not promo.active:
                return render_to_response('registration_closed.html', {'summit': summit})
        else:
            return render_to_response('registration_closed.html', {'summit': summit}) 
        if user_is_dd(user):
            debian_role = DebianRole(1)
        else:
            debian_role = DebianRole(7)
        profile = UserProfile(
              user=user,
              reconfirm = True,
              badge_full=user.first_name + ' ' + user.last_name,
              contact_email=user.email,
              public_email=user.email,
              role_debian=debian_role,
              role_debconf=DebConfRole(1),
              start_utc=summit.date_arrival,
              end_utc=summit.date_departure,
              summit=summit
        )

    try:
        sponsorship = Sponsorship.objects.get(summit=summit, user=user)
    except Sponsorship.DoesNotExist:
        if summit.state == 'sponsor' or summit.state == 'setup':
            sponsorship = Sponsorship(
                  user=user,
                  real_name=user.first_name + ' ' + user.last_name,
                  userprofile=profile,
                  summit=summit
            )
        else:
	    sponsorship = None

    if request.method == 'POST':
        form = UserProfileForm(request.POST, request.FILES, instance=profile)
        if summit.state == 'sponsor' or summit.state == 'setup':
            sponsform = SponsorshipForm(request.POST, request.FILES, instance=sponsorship)
            if not sponsform.is_valid():
                errors.append(sponsform.errors)

        if not errors and form.is_valid():
            if promo and promo.active:
                promo.active = False
                promo.save()
            else:
                return render_to_response('registration_closed.html', {'summit': summit})

            form.save()
            if summit.state == 'sponsor' or summit.state == 'setup':
                sponsorship_instance = sponsform.save(commit=False)
                sponsorship_instance.userprofile = profile
                sponsorship_instance.save()
            return HttpResponseRedirect(
                reverse(
                    'debconf_website.views.registered',
                    args=(summit.name,)
                )
            )
    else:
        form = UserProfileForm(instance=profile)
        if summit.state == 'sponsor' or summit.state == 'setup':
            sponsform = SponsorshipForm(instance=sponsorship)
        else:
	    if sponsorship:
		sponsform = SponsorshipReadOnlyForm(instance=sponsorship)

    context = {'form': form,
               'sponsform': sponsform,
               'summit': summit, 'promocode': promocode}

    return render_to_response('registration.html', context,
                              RequestContext(request))

@login_required
@summit_required
def registered(request, summit, attendee):
    profile = UserProfile.objects.get(attendee_ptr=attendee)
    try:
        sponsorship = Sponsorship.objects.get(userprofile=profile)
    except Sponsorship.DoesNotExist:
        sponsorship = None

    context = {
        'summit': summit,
        'profile': profile,
        'sponsorship': sponsorship,
    }
    return render_to_response("registered.html", context,
                              context_instance=RequestContext(request))

@permission_required('sponsor.add_sponsorshipscore')
def review_list(request, summit_name):
    summit = get_object_or_404(Summit, name=summit_name)
    sponsorships = sorted(Sponsorship.objects.filter(
                              summit=summit
                          ).exclude(
                              needs_food=False, needs_travel=False,
                              needs_accomodation=False
                          ).exclude(
                              user=request.user
                          ),
                          key=lambda x: (x.numscores > 0 and -1 or 0,
                                         -x.score, x.user.username, ))
    for x in sponsorships:
        if x.scored_by(request.user):
            x.already_scored = True

    return render_to_response("sponsor/review-index.html",
                              {'summit': summit,
                               'sponsorships': sponsorships},
                              context_instance=RequestContext(request))

@permission_required('sponsor.add_sponsorshipscore')
def review_list_csv(request, summit_name):
    summit = get_object_or_404(Summit, name=summit_name)

    csv = ('"Name","Email","User","Score","Arrival","Departure","Arrived","Accommodation","DebCamp","Travel",'
           '"Job","Use","Costs","Needed","FinancialNeed","Benefit","Info","Days","City","Country"\n')

    sponsorships = sorted(Sponsorship.objects.filter(
                              summit=summit
                          ).exclude(
                              needs_debcamp=False, needs_travel=False,
                              needs_accomodation=False
                          ),
                          key=lambda x: (x.numscores > 0 and -1 or 0,
                                         -x.score, x.user.username, ))
    for sponsorship in sponsorships:
        start_date = sponsorship.userprofile.start_utc
        end_date = sponsorship.userprofile.end_utc
        # For purposes of counting nights, always measure from the check-in
        # time of 2pm.
        if start_date.hour < 14:
            start_date = start_date.replace(hour=14)
        if end_date.hour == 0:
            end_date = end_date - datetime.timedelta(1)
            end_date = end_date.replace(hour=14)
        elif end_date.hour > 14:
            end_date = end_date.replace(hour=14)
        csv += '"%s","%s","%s",%f,"%s","%s","%s","%s","%s","%s","%s","%s",%d,%d,"%s","%s","%s","%i","%s","%s"\n' \
            % (
               sponsorship.user.get_full_name(),
               sponsorship.userprofile.contact_email,
               sponsorship.user.username,
               sponsorship.score,
               sponsorship.userprofile.start_utc,
               sponsorship.userprofile.end_utc,
               sponsorship.userprofile.arrived and "Y" or "N",
               sponsorship.needs_accomodation and "Y" or "N",
               sponsorship.needs_debcamp and "Y" or "N",
               sponsorship.needs_travel and "Y" or "N",
               sponsorship.job,
               sponsorship.use,
               sponsorship.needs_travel and sponsorship.costs or 0,
               sponsorship.needs_travel and sponsorship.needed or 0,
               sponsorship.financial_need,
               sponsorship.benefit,
               sponsorship.further_info,
               (end_date - start_date).days,
               sponsorship.userprofile.city_state,
               sponsorship.userprofile.country
              )

    response = HttpResponse(csv, content_type='text/csv; charset=utf-8')
    response['Content-Disposition'] = 'attachment; filename="sponsorship-%s.csv"' % summit_name
    return response


@permission_required('sponsor.add_sponsorshipscore')
def review(request, summit_name, sponsorship_id):
    sponsorship = get_object_or_404(Sponsorship, id=sponsorship_id)
    scores = sponsorship.sponsorshipscore_set.exclude()
    try:
        score = sponsorship.sponsorshipscore_set.get(user=request.user)
    except ObjectDoesNotExist:
        score = None

    if request.POST:
        if score is None:
            score = sponsorship.sponsorshipscore_set.create(user=request.user,
                                                            score=0,
                                                            comment="")

        form = SponsorshipScoreForm(request.POST, instance=score)
        form.save()

        return HttpResponseRedirect(
            '/%s/sponsorship/review' % sponsorship.summit.name)

    else:
        form = SponsorshipScoreForm(instance=score)

        return render_to_response("sponsor/review.html",
                                  {'sponsorship': sponsorship,
                                   'score': score,
                                   'scores': scores,
                                   'form': form},
                                  context_instance=RequestContext(request))
@login_required
@summit_only_required
@permission_required('debconf_website.manage_registration')
def badge_list_csv(request, summit_name):
    summit = get_object_or_404(Summit, name=summit_name)

    csv = ('"ID","Name","Nickname",food_accommodatio_id,"Food_Accommodation","email"\n')

    userprofiles = sorted(UserProfile.objects.filter(
                              summit=summit, attend=True),
                          key=id)
    for userprofile in userprofiles:
        email = userprofile.contact_email
        if not email:
            if userprofile.public_email:
                email = userprofile.public_email
            else:
                email = userprofile.user.email
        csv += '%s,"%s","%s",%s,"%s","%s"\n' \
            % (
               userprofile.id,
               userprofile.badge_full,
               userprofile.badge_nick,
               userprofile.food_accommodation.id,
               userprofile.food_accommodation.type,
               email,
              )
    response = HttpResponse(csv, content_type='text/csv; charset=utf-8')
    response['Content-Disposition'] = 'attachment; filename="badges-%s.csv"' % summit_name
    return response


@login_required
@summit_only_required
@permission_required('debconf_website.manage_registration')
def frontdesk_attendance(request, summit):
    AttendanceFormSet = modelformset_factory(UserProfile,
                                             fields=(
                                                 'arrived',
                                                 'received_tshirt',
                                                 'received_bag',
                                                 "got_swag2",
                                             ),
                                             extra=0)

    if request.POST:
        formset = AttendanceFormSet(request.POST, request.FILES)
        if formset.is_valid():
            formset.save()
            return HttpResponseRedirect(
                reverse(
                    'debconf_website.views.frontdesk_attendance',
                    args=(summit,)
                ))
    else:
        formset = AttendanceFormSet(queryset=UserProfile
                                    .objects
                                    .filter(summit=summit, attend=True)
                                    .order_by('id'))

    return render_to_response('frontdesk/attendance.html',
                              {'summit': summit,
                               'formset': formset},
                              context_instance=RequestContext(request))

class FrontDeskUserProfileForm(UserProfileForm):
    class Meta(UserProfileForm.Meta):
        fields = UserProfileForm.Meta.fields + ('arrived',
                                                'received_tshirt',
                                                'received_bag')

class FrontDeskSponsorshipForm(SponsorshipForm, ReadOnlyFormMixin):
    def __init__(self, *args, **kargs):
        super(FrontDeskSponsorshipForm, self).__init__(*args, **kargs)
        for field_name, field in self.fields.iteritems():
            if (field_name in [ 'use', 'benefit', 'rationale', 'costs',
                                'needed', 'financial_need', 'further_info' ]):
                field.widget = HiddenInput()

@login_required
@summit_only_required
@permission_required('debconf_website.manage_registration')
def frontdesk_registration(request, summit, userprofile_id=None):
    if userprofile_id:
        profile = get_object_or_404(UserProfile, id=userprofile_id)
        try:
            sponsorship = Sponsorship.objects.get(userprofile=profile)
        except Sponsorship.DoesNotExist:
            sponsorship = None
    else:
        try:
            nobody = User.objects.get(username='nobody')
        except User.DoesNotExist:
            nobody = User(username='nobody', is_active=False)
            nobody.save()
        start_utc = now()
        start_utc -= datetime.timedelta(minutes=start_utc.minute % 15,
                                        seconds=start_utc.second,
                                        microseconds=start_utc.microsecond)
        profile = UserProfile(
            user=nobody,
            subscribe=False,
            reconfirm=True,
            role_debconf=DebConfRole(6),
            start_utc=start_utc,
            end_utc=summit.date_departure,
            arrived=True,
            summit=summit
        )
        sponsorship = None

    if request.method == 'POST':
        form = FrontDeskUserProfileForm(request.POST, request.FILES, instance=profile)
        if sponsorship:
            sponsform = FrontDeskSponsorshipForm(request.POST,
                                                 request.FILES,
                                                 instance=sponsorship)
        else:
            sponsform = None

        if form.is_valid():
            valid = True
            if sponsform:
                if sponsform.is_valid():
                    sponsform.save()
                else:
                    valid = False
            if valid:
                form.save()
                return HttpResponseRedirect(
                    reverse(
                        'debconf_website.views.frontdesk_attendance',
                        args=(summit,)
                    ))
    else:
        form = FrontDeskUserProfileForm(instance=profile)
        if sponsorship:
            sponsform = FrontDeskSponsorshipForm(instance=sponsorship)
        else:
            sponsform = None

    return render_to_response('frontdesk/registration.html',
                              {'summit': summit,
                               'form': form,
                               'sponsform': sponsform},
                              context_instance=RequestContext(request))

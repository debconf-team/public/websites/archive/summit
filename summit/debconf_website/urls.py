from django.conf import settings
from django.conf.urls.defaults import *
from django.contrib import admin

import debconf_website
import volunteers.views

admin.autodiscover()

if settings.DEBUG or settings.SERVE_STATIC:
    urlpatterns = patterns(
        '',
        (r'^debconf-website/media/(?P<path>.*)$',
         'django.views.static.serve',
         {'document_root': settings.THEME_MEDIA}),
    )

urlpatterns += patterns(
    '',

    # put at the beginning, so that it would not be confused with summit_name/edition_name "admin/"
    (r'^admin/', admin.site.urls),


    url(r'^(?P<summit_name>[\w-]+)/sponsorship/$', 'debconf_website.views.registration'),
    url(r'^(?P<summit_name>[\w-]+)/registration/$', 'debconf_website.views.registration'),
    url(r'^(?P<summit_name>[\w-]+)/registered/$', 'debconf_website.views.registered'),
    url(r'^(?P<summit_name>[\w-]+)/sponsorship/review/$', 'debconf_website.views.review_list'),
    url(r'^(?P<summit_name>[\w-]+)/sponsorship/review/(?P<sponsorship_id>[0-9]+)$',
        'debconf_website.views.review'),
    url(r'^(?P<summit_name>[\w-]+)/sponsorship/export/$', 'debconf_website.views.review_list_csv'),
    url(r'^(?P<summit_name>[\w-]+)/frontdesk/badges/$', 'debconf_website.views.badge_list_csv'),
    url(r'^(?P<summit_name>[\w-]+)/frontdesk/attendance/$',
        'debconf_website.views.frontdesk_attendance'),
    url(r'^(?P<summit_name>[\w-]+)/frontdesk/registration/(?P<userprofile_id>[0-9]+)$',
        'debconf_website.views.frontdesk_registration'),
    url(r'^(?P<summit_name>[\w-]+)/frontdesk/registration/register$',
        'debconf_website.views.frontdesk_registration'),
    url(r'^(?P<summit_name>[\w-]+)/stats/bydays$', 'debconf_website.stats.by_days'),


    url(r'^(?P<edition_name>[\w-]+)/volunteers/$', volunteers.views.promo, name='promo'),
    url(r'^(?P<edition_name>[\w-]+)/volunteers/faq/', volunteers.views.faq, name='faq'),
    url(r'^(?P<edition_name>[\w-]+)/volunteers/signup', volunteers.views.signup, name='signup'),
    url(r'^(?P<edition_name>[\w-]+)/volunteers/volunteer/(?P<username>(?!signout|signup|signin)[\.\w-]+)/$', volunteers.views.profile_detail, name='profile_detail'),
    url(r'^(?P<edition_name>[\w-]+)/volunteers/volunteer/(?P<username>[\.\w-]+)/edit/$', volunteers.views.profile_edit, name='userena_profile_edit'),
    url(r'^(?P<edition_name>[\w-]+)/volunteers/page/(?P<page>[0-9]+)/$', volunteers.views.ProfileListView.as_view(), name='userena_profile_list_paginated'),
    url(r'^(?P<edition_name>[\w-]+)/volunteers/list$', volunteers.views.ProfileListView.as_view(), name='userena_profile_list'),
#    url(r'^volunteers/', include('userena.urls')),
#    url(r'^messages/', include('userena.contrib.umessages.urls')),
    # other urls:
    url(r'^(?P<edition_name>[\w-]+)/volunteers/tasks/(?P<username>[\.\w-]+)', volunteers.views.task_list_detailed, name='task_list_detailed'),
    url(r'^(?P<edition_name>[\w-]+)/volunteers/task/(?P<task_id>\d+)/$', volunteers.views.task_detailed, name='task_detailed'),
    url(r'^(?P<edition_name>[\w-]+)/volunteers/talk/(?P<talk_id>\d+)/$', volunteers.views.talk_detailed, name='talk_detailed'),
    url(r'^(?P<edition_name>[\w-]+)/volunteers/tasks/', volunteers.views.task_list, name='task_list'),
    url(r'^(?P<edition_name>[\w-]+)/volunteers/schedule/(?P<date>[\d-]+)/$', volunteers.views.task_grid, name='task_grid'), 
    url(r'^(?P<edition_name>[\w-]+)/volunteers/talks/', volunteers.views.talk_list, name='talk_list'),
    url(r'^(?P<edition_name>[\w-]+)/volunteers/category_schedule/', volunteers.views.category_schedule_list, name='category_schedule_list'),
    url(r'^(?P<edition_name>[\w-]+)/volunteers/task_schedule/(?P<template_id>\d+)/$', volunteers.views.task_schedule, name='task_schedule'),
    url(r'^(?P<edition_name>[\w-]+)/volunteers/task_schedule_csv/(?P<template_id>\d+)/$', volunteers.views.task_schedule_csv, name='task_schedule_csv'),


    url(r'', include('summit.urls')),

)

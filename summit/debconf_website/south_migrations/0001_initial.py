# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Sex'
        db.create_table('debconf_website_sex', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('sex', self.gf('django.db.models.fields.CharField')(max_length=20)),
        ))
        db.send_create_signal('debconf_website', ['Sex'])

        # Adding model 'DebianRole'
        db.create_table('debconf_website_debianrole', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('role', self.gf('django.db.models.fields.CharField')(max_length=80)),
        ))
        db.send_create_signal('debconf_website', ['DebianRole'])

        # Adding model 'DebConfRole'
        db.create_table('debconf_website_debconfrole', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('role', self.gf('django.db.models.fields.CharField')(max_length=80)),
        ))
        db.send_create_signal('debconf_website', ['DebConfRole'])

        # Adding model 'TShirtSize'
        db.create_table('debconf_website_tshirtsize', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('size', self.gf('django.db.models.fields.CharField')(max_length=40)),
        ))
        db.send_create_signal('debconf_website', ['TShirtSize'])

        # Adding model 'RegistrationLevel'
        db.create_table('debconf_website_registrationlevel', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('level', self.gf('django.db.models.fields.CharField')(max_length=40)),
        ))
        db.send_create_signal('debconf_website', ['RegistrationLevel'])

        # Adding model 'Diet'
        db.create_table('debconf_website_diet', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('preference', self.gf('django.db.models.fields.CharField')(max_length=40)),
        ))
        db.send_create_signal('debconf_website', ['Diet'])

        # Adding model 'UserProfile'
        db.create_table('debconf_website_userprofile', (
            ('attendee_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['schedule.Attendee'], unique=True, primary_key=True)),
            ('badge_full', self.gf('django.db.models.fields.CharField')(max_length=80)),
            ('badge_nick', self.gf('django.db.models.fields.CharField')(max_length=20, blank=True)),
            ('sex', self.gf('django.db.models.fields.related.ForeignKey')(default=3, to=orm['debconf_website.Sex'], on_delete=models.PROTECT)),
            ('public_image', self.gf('django.db.models.fields.files.ImageField')(max_length=100, blank=True)),
            ('role_debian', self.gf('django.db.models.fields.related.ForeignKey')(default=7, to=orm['debconf_website.DebianRole'], on_delete=models.PROTECT)),
            ('role_debconf', self.gf('django.db.models.fields.related.ForeignKey')(default=7, to=orm['debconf_website.DebConfRole'], on_delete=models.PROTECT)),
            ('tshirt_size', self.gf('django.db.models.fields.related.ForeignKey')(default=10, to=orm['debconf_website.TShirtSize'], on_delete=models.PROTECT)),
            ('recording_ok', self.gf('django.db.models.fields.IntegerField')(default=1, max_length=1)),
            ('attend', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('reconfirm', self.gf('django.db.models.fields.NullBooleanField')(null=True, blank=True)),
            ('registration_level', self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['debconf_website.RegistrationLevel'], on_delete=models.PROTECT)),
            ('diet', self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['debconf_website.Diet'], on_delete=models.PROTECT)),
            ('daytrip', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('assassins', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('child_care', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('subscribe', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('contact_email', self.gf('django.db.models.fields.CharField')(max_length=80, blank=True)),
            ('public_email', self.gf('django.db.models.fields.CharField')(max_length=80, blank=True)),
            ('telephone', self.gf('django.db.models.fields.CharField')(max_length=40, blank=True)),
            ('address', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('city_state', self.gf('django.db.models.fields.CharField')(max_length=40, blank=True)),
            ('postcode', self.gf('django.db.models.fields.CharField')(max_length=40, blank=True)),
            ('country', self.gf('django_countries.fields.CountryField')(max_length=2, blank=True)),
            ('emergency_name', self.gf('django.db.models.fields.CharField')(max_length=40, blank=True)),
            ('emergency_contact', self.gf('django.db.models.fields.CharField')(max_length=80, blank=True)),
            ('arrival_needs', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('departure_needs', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('createTimestamp', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('modifyTimestamp', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
        ))
        db.send_create_signal('debconf_website', ['UserProfile'])

        # Adding model 'Sponsorship'
        db.create_table('debconf_website_sponsorship', (
            ('sponsorship_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['sponsor.Sponsorship'], unique=True, primary_key=True)),
            ('needs_food', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('use', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('benefit', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('rationale', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('userprofile', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['debconf_website.UserProfile'], unique=True)),
            ('costs', self.gf('django.db.models.fields.CharField')(max_length=40, blank=True)),
            ('needed', self.gf('django.db.models.fields.CharField')(max_length=40, blank=True)),
            ('roommate', self.gf('django.db.models.fields.CharField')(max_length=80, blank=True)),
        ))
        db.send_create_signal('debconf_website', ['Sponsorship'])


    def backwards(self, orm):
        # Deleting model 'Sex'
        db.delete_table('debconf_website_sex')

        # Deleting model 'DebianRole'
        db.delete_table('debconf_website_debianrole')

        # Deleting model 'DebConfRole'
        db.delete_table('debconf_website_debconfrole')

        # Deleting model 'TShirtSize'
        db.delete_table('debconf_website_tshirtsize')

        # Deleting model 'RegistrationLevel'
        db.delete_table('debconf_website_registrationlevel')

        # Deleting model 'Diet'
        db.delete_table('debconf_website_diet')

        # Deleting model 'UserProfile'
        db.delete_table('debconf_website_userprofile')

        # Deleting model 'Sponsorship'
        db.delete_table('debconf_website_sponsorship')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'ordering': "['username']", 'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'debconf_website.debconfrole': {
            'Meta': {'ordering': "('id',)", 'object_name': 'DebConfRole'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'role': ('django.db.models.fields.CharField', [], {'max_length': '80'})
        },
        'debconf_website.debianrole': {
            'Meta': {'ordering': "('id',)", 'object_name': 'DebianRole'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'role': ('django.db.models.fields.CharField', [], {'max_length': '80'})
        },
        'debconf_website.diet': {
            'Meta': {'ordering': "('preference',)", 'object_name': 'Diet'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'preference': ('django.db.models.fields.CharField', [], {'max_length': '40'})
        },
        'debconf_website.registrationlevel': {
            'Meta': {'ordering': "('level',)", 'object_name': 'RegistrationLevel'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.CharField', [], {'max_length': '40'})
        },
        'debconf_website.sex': {
            'Meta': {'ordering': "('sex',)", 'object_name': 'Sex'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'sex': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        'debconf_website.sponsorship': {
            'Meta': {'object_name': 'Sponsorship', '_ormbases': ['sponsor.Sponsorship']},
            'benefit': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'costs': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'needed': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'needs_food': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'rationale': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'roommate': ('django.db.models.fields.CharField', [], {'max_length': '80', 'blank': 'True'}),
            'sponsorship_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['sponsor.Sponsorship']", 'unique': 'True', 'primary_key': 'True'}),
            'use': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'userprofile': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['debconf_website.UserProfile']", 'unique': 'True'})
        },
        'debconf_website.tshirtsize': {
            'Meta': {'ordering': "('id',)", 'object_name': 'TShirtSize'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'size': ('django.db.models.fields.CharField', [], {'max_length': '40'})
        },
        'debconf_website.userprofile': {
            'Meta': {'ordering': "('user__username',)", 'object_name': 'UserProfile', '_ormbases': ['schedule.Attendee']},
            'address': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'arrival_needs': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'assassins': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'attend': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'attendee_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['schedule.Attendee']", 'unique': 'True', 'primary_key': 'True'}),
            'badge_full': ('django.db.models.fields.CharField', [], {'max_length': '80'}),
            'badge_nick': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'child_care': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'city_state': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'contact_email': ('django.db.models.fields.CharField', [], {'max_length': '80', 'blank': 'True'}),
            'country': ('django_countries.fields.CountryField', [], {'max_length': '2', 'blank': 'True'}),
            'createTimestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'daytrip': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'departure_needs': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'diet': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'to': "orm['debconf_website.Diet']", 'on_delete': 'models.PROTECT'}),
            'emergency_contact': ('django.db.models.fields.CharField', [], {'max_length': '80', 'blank': 'True'}),
            'emergency_name': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'modifyTimestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'postcode': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'public_email': ('django.db.models.fields.CharField', [], {'max_length': '80', 'blank': 'True'}),
            'public_image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'reconfirm': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'recording_ok': ('django.db.models.fields.IntegerField', [], {'default': '1', 'max_length': '1'}),
            'registration_level': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'to': "orm['debconf_website.RegistrationLevel']", 'on_delete': 'models.PROTECT'}),
            'role_debconf': ('django.db.models.fields.related.ForeignKey', [], {'default': '7', 'to': "orm['debconf_website.DebConfRole']", 'on_delete': 'models.PROTECT'}),
            'role_debian': ('django.db.models.fields.related.ForeignKey', [], {'default': '7', 'to': "orm['debconf_website.DebianRole']", 'on_delete': 'models.PROTECT'}),
            'sex': ('django.db.models.fields.related.ForeignKey', [], {'default': '3', 'to': "orm['debconf_website.Sex']", 'on_delete': 'models.PROTECT'}),
            'subscribe': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'telephone': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'tshirt_size': ('django.db.models.fields.related.ForeignKey', [], {'default': '10', 'to': "orm['debconf_website.TShirtSize']", 'on_delete': 'models.PROTECT'})
        },
        'schedule.attendee': {
            'Meta': {'ordering': "('user__username', 'summit')", 'object_name': 'Attendee'},
            'crew': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_column': "'crew'"}),
            'end_utc': ('django.db.models.fields.DateTimeField', [], {'db_column': "'end'"}),
            'from_launchpad': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'secret_key_id': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'}),
            'start_utc': ('django.db.models.fields.DateTimeField', [], {'db_column': "'start'"}),
            'summit': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['schedule.Summit']"}),
            'tracks': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['schedule.Track']", 'symmetrical': 'False', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        },
        'schedule.summit': {
            'Meta': {'ordering': "('name',)", 'object_name': 'Summit'},
            'date_end': ('django.db.models.fields.DateField', [], {'null': 'True'}),
            'date_start': ('django.db.models.fields.DateField', [], {'null': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'max_length': '2047', 'blank': 'True'}),
            'etherpad': ('django.db.models.fields.URLField', [], {'default': "'http://pad.ubuntu.com/'", 'max_length': '75'}),
            'hashtag': ('django.db.models.fields.CharField', [], {'max_length': '25', 'blank': 'True'}),
            'help_text': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_update': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'location': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'managers': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'managers'", 'blank': 'True', 'to': "orm['auth.User']"}),
            'name': ('summit.schedule.fields.NameField', [], {'max_length': '50'}),
            'qr': ('django.db.models.fields.URLField', [], {'default': "''", 'max_length': '100', 'blank': 'True'}),
            'schedulers': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'schedulers'", 'blank': 'True', 'to': "orm['auth.User']"}),
            'sites': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['sites.Site']", 'symmetrical': 'False'}),
            'state': ('django.db.models.fields.CharField', [], {'default': "u'sponsor'", 'max_length': '10'}),
            'timezone': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'virtual_summit': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'schedule.track': {
            'Meta': {'ordering': "('summit', 'title', 'slug')", 'object_name': 'Track'},
            'allow_adjacent_sessions': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'color': ('django.db.models.fields.CharField', [], {'default': "'FFFFFF'", 'max_length': '6'}),
            'description': ('django.db.models.fields.TextField', [], {'max_length': '1000', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'slug': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'summit': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['schedule.Summit']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'sites.site': {
            'Meta': {'ordering': "('domain',)", 'object_name': 'Site', 'db_table': "'django_site'"},
            'domain': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'sponsor.sponsorship': {
            'Meta': {'object_name': 'Sponsorship'},
            'about': ('django.db.models.fields.TextField', [], {'max_length': '1000'}),
            'country': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'diet': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'further_info': ('django.db.models.fields.TextField', [], {'max_length': '1000', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'needs_accomodation': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'needs_travel': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'real_name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'summit': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['schedule.Summit']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"}),
            'video_agreement': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'would_crew': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        }
    }

    complete_apps = ['debconf_website']
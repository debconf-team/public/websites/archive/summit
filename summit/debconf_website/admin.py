from debconf_website.models import *
import sponsor.admin
from django.contrib import admin

class UserProfileAdmin(admin.ModelAdmin):
    list_display = ('pk', 'summit', 'user', 'badge_full', 'attend', 'reconfirm', 'role_debconf')
    list_display_links = ('user', )
    list_filter = ('summit', 'role_debconf', 'attend', 'reconfirm', 'registration_level', 'food_accommodation', 'diet', 'fee_paid')
    search_fields = (
        'user__username',
        'user__first_name',
        'user__last_name',
        'user__email',
        'badge_full'
    )


class SponsorshipAdmin(admin.ModelAdmin):
    list_display = ('pk', 'summit', 'user', 'real_name', 'needs_travel', 'needs_accomodation', 'needs_food', 'needs_debcamp', 'volunteering')
    list_display_links = ('real_name', 'user', )
    list_filter = ('summit', 'needs_travel', 'needs_accomodation', 'needs_food', 'needs_debcamp', 'volunteering', 'got_accommodation', 'got_debcamp', 'got_travel')
    search_fields = (
        'user__username',
        'real_name',
        'user__first_name',
        'user__last_name',
        'user__email'
    )
    inlines = (sponsor.admin.SponsorshipScoreInline, )


class FoodAccommodationAdmin(admin.ModelAdmin):
    list_display = ('pk', 'type', 'visible_in_form', 'still_selectable')
    list_display_links = ('type', 'still_selectable', )

class PromotionCodeAdmin(admin.ModelAdmin):
    list_display = ('pk', 'code', 'active')
    list_display_links = ('pk', 'code', )


admin.site.register(Sex)
admin.site.register(DebianRole)
admin.site.register(DebConfRole)
admin.site.register(TShirtSize)
admin.site.register(RegistrationLevel)
admin.site.register(Diet)
admin.site.register(PromotionCode, PromotionCodeAdmin)
admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(Sponsorship, SponsorshipAdmin)
admin.site.register(SponsorshipFinancialNeed)
admin.site.register(SponsorshipJob)
admin.site.register(FoodAccommodation, FoodAccommodationAdmin)

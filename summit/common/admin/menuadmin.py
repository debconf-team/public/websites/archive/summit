from django.contrib import admin
from common.models import Menu, MenuItem

class MenuItemInline(admin.TabularInline):
    model = MenuItem

class MenuAdmin(admin.ModelAdmin):
    inlines = [MenuItemInline,]
    list_display = ('name', 'slug', 'base_url')

admin.site.register(Menu, MenuAdmin)

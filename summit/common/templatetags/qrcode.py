# Snippet from http://djangosnippets.org/snippets/1494/
# Generate QR Code image from a string with the Google charts API
# 
# http://code.google.com/intl/fr-FR/apis/chart/types.html#qrcodes
# 
# Exemple usage in a template
# 
# {{ my_string|qrcode:"my alt" }}
# will return the image tag with
# 
# src: http://chart.apis.google.com/chart?chs=150x150&amp;cht=qr&amp;chl=my_string&amp;choe=UTF-8
# Author:johnnoone
# Posted:May 6, 2009
# Language:Python
# Django Version:1.0

import urllib
from django import template
from django.template.defaultfilters import stringfilter
from django.utils.html import conditional_escape
from django.utils.safestring import mark_safe
from django.conf import settings

register = template.Library()

@register.filter
@stringfilter
def qrcode(value, alt=None):
    """
    Generate QR Code image from a string with the Google charts API
    
    http://code.google.com/intl/fr-FR/apis/chart/types.html#qrcodes
    
    Exemple usage --
    {{ my_string|qrcode:"my alt" }}
    
    <img src="http://chart.apis.google.com/chart?chs=150x150&amp;cht=qr&amp;chl=my_string&amp;choe=UTF-8" alt="my alt" />
    """
    
    url = conditional_escape("http://chart.apis.google.com/chart?%s" % \
            urllib.urlencode({'chs':'150x150', 'cht':'qr', 'chl':value, 'choe':'UTF-8'}))
    alt = conditional_escape(alt or value)
    
    return mark_safe(u"""<img class="qrcode" src="%s" width="150" height="150" alt="%s" />""" % (url, alt))

def build_qrhere(parser, token):
    """
    {% qrhere %}
    
    Returns an <img> with a QR Code pointing to the current URL
    """
    return QRHere()

class QRHere(template.Node):
    def __init__(self):
        pass

    def render(self, context):
        if 'request' not in context:
            return ''
        current_url = '%s/%s' % (settings.SITE_ROOT, context['request'].path)
        url = conditional_escape("http://chart.apis.google.com/chart?%s" % \
            urllib.urlencode({'chs':'150x150', 'cht':'qr', 'chl':current_url, 'choe':'UTF-8'}))
        return mark_safe(u"""<img class="qrcode" src="%s" width="150" height="150" />""" % url)

register.tag('qrhere', build_qrhere)

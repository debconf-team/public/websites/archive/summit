# The Summit Scheduler web application
# Copyright (C) 2008 - 2013 Ubuntu Community, Canonical Ltd
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# context processors for The Summit Scheduler
# see http://docs.djangoproject.com/en/dev/ref/settings/#setting-TEMPLATE_CONTEXT_PROCESSORS

from schedule.models.summitmodel import Summit

from django.conf import settings

from common.models import Menu
import version


def url_base(request):
    url = request.get_full_path().split('/')
    return {'url_base': url[1]}


def next_summit(request):
    return {'next_summit': Summit.on_site.next()}


def login_redirect(request):
    return {'login_next': request.get_full_path()}


def login_url(request):
    return {'login_url': settings.LOGIN_URL}


def summit_version(request):
    """
    add The Summit Scheduler version to template context processor.
    """
    return {'summit_version': version.version, 'summit_revno': version.revno}


def site_menu(request):
    """
    Adds the site's menu to the template context
    """
    try:
        return {'main_menu': Menu.on_site.filter(name='main_menu', base_url= request.path.split('/')[1])[0].slug}
    except IndexError:
        return {}


def track_display_name(request):
    """
    Adds the track display name from settings
    """
    display = getattr(settings, 'TRACK_DISPLAY_NAME', 'Track')
    display_plural = getattr(
        settings,
        'TRACK_DISPLAY_NAME_PLURAL',
        display+'s'
    )
    return {
        'TRACK_DISPLAY_NAME': display,
        'TRACK_DISPLAY_NAME_PLURAL': display_plural
    }

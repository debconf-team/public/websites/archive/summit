var Schedule = {
    init: function() {
	document.onmousemove = Schedule.mouseMove;
	document.onmouseup   = Schedule.mouseUp;

	for (var i = 0; i < Data.meetings.length; i++) {
	    var div = document.getElementById(Data.meetings[i]);
	    div.onmousedown = Schedule.mouseDown;
	}
    },


    // Return the mouse position
    mousePos: function(ev) {
	return { x: ev.pageX, y: ev.pageY };
    },

    // Return the offset an object from the mouse position
    mouseOffset: function(object, ev) {
	var docPos   = Schedule.objectPosition(object);
	var mousePos = Schedule.mousePos(ev);

	return { x: mousePos.x - docPos.x, y: mousePos.y - docPos.y };
    },

    // Return the position of an object
    objectPosition: function(object) {
	var left = 0;
	var top  = 0;

	while (object.offsetParent) {
	    left   += object.offsetLeft;
	    top    += object.offsetTop;
	    object  = object.offsetParent;
	}

	left += object.offsetLeft;
	top  += object.offsetTop;

	return { x: left, y: top };
    },

    // Return the object under the mouse cursor
    objectAt: function(ev, objects) {
	var mousePos = Schedule.mousePos(ev);

	// Check the dock
	if (mousePos.x - window.pageXOffset > document.getElementById("dock").offsetLeft)
	    return document.getElementById("dock");

	for (var i = 0; i < objects.length; i++) {
	    // Ignore targets that are full
	    if (Data.slot[objects[i]])
		continue;

	    var target       = document.getElementById(objects[i]);
	    var targetPos    = Schedule.objectPosition(target);
	    var targetWidth  = parseInt(target.offsetWidth);
	    var targetHeight = parseInt(target.offsetHeight);

	    if (   (mousePos.x > targetPos.x)
		&& (mousePos.x < targetPos.x + targetWidth)
		&& (mousePos.y > targetPos.y)
		&& (mousePos.y < targetPos.y + targetHeight))
	        return target;
	}

	return null;
    },

    // Object we're dragging
    drag: null,

    // Begin a drag; save the details of the object so we can revert and
    // attach the object to the mouse cursor.
    mouseDown: function(ev) {
	if (Schedule.drag)
	    Schedule.revert(Schedule.drag);

	Schedule.drag = {
	    object:      this,
	    offset:      Schedule.mouseOffset(this, ev),
	    parentNode:  this.parentNode,
	    nextSibling: this.nextSibling,
	    position:    this.style.position,
	    left:        this.style.left,
	    top:         this.style.top,
	    height:      this.style.height,
	    width:       this.style.width,
	    zIndex:      this.style.zIndex,
	    target:      null,
	}

	var mousePos = Schedule.mousePos(ev);
	var left     = mousePos.x - Schedule.drag.offset.x;
	var top      = mousePos.y - Schedule.drag.offset.y;
	var height   = Data.meetingSlots[Schedule.drag.object.id] * 120 - 12;
	var width    = 100;

	Schedule.drag.parentNode.removeChild(Schedule.drag.object);
	document.body.insertBefore(Schedule.drag.object,
				   document.body.firstChild);

	if (Schedule.drag.parentNode.id == 'dock') {
	    // Temporary fix!
	    Schedule.drag.object.style.position = 'absolute';
	    Schedule.drag.offset.x = 50;
	    Schedule.drag.offset.y = 20;
	    left = mousePos.x - Schedule.drag.offset.x;
	    top = mousePos.y - Schedule.drag.offset.y;
	} else {
	    Schedule.drag.object.style.position = 'absolute';

	    // We may be resizing the object, so make sure we're still within
	    // the new bounds (doesn't apply to dock since those already have
	    // the expected size - and fixed buggers it up anyway)
	    if (Schedule.drag.offset.x > width + 12) {
		Schedule.drag.offset.x = width / 2;
		left = mousePos.x - Schedule.drag.offset.x;
	    }
	    if (Schedule.drag.offset.y > height + 12) {
		Schedule.drag.offset.y = height / 2;
		top = mousePos.x - Schedule.drag.offset.y;
	    }
	}

	Schedule.drag.object.style.left      = left + 'px';
	Schedule.drag.object.style.top       = top + 'px';
	Schedule.drag.object.style.height    = height + 'px';
	Schedule.drag.object.style.minHeight = height + 'px';
	Schedule.drag.object.style.width     = width + 'px';
	Schedule.drag.object.style.zIndex    = 20;

	// When dragging out of a slot, make the slot's own div visible
	var old_slot = Data.meeting[Schedule.drag.object.id];
	if (old_slot) {
	    var slots = Data.meetingSlots[Schedule.drag.object.id];
	    var div = document.getElementById(old_slot);
	    while (div && slots--) {
		div.style.visibility = 'visible';

		if (Data.slotNext[div.id]) {
		    div = document.getElementById(Data.slotNext[div.id]);
		} else {
		    div = null;
		}
	    }
	}

	return false;
    },

    // Moving mouse during a drag, keep the object attached to the mouse
    // cursor and perform pre-lighting on possible drag targets
    mouseMove: function(ev) {
	if (! Schedule.drag)
	    return;

	var mousePos = Schedule.mousePos(ev);

	var left = mousePos.x - Schedule.drag.offset.x;
	var top  = mousePos.y - Schedule.drag.offset.y;

	Schedule.drag.object.style.left = left + 'px';
	Schedule.drag.object.style.top  = top + 'px';

	var target = Schedule.objectAt(ev, Data.slots);
	if (target != Schedule.drag.target) {
	    Schedule.setError(Schedule.drag.object, null, null);

	    if (Schedule.drag.target && Schedule.drag.target.id == 'dock') {
		Schedule.drag.target.className = Schedule.drag.target.className.replace(/\boverhead\b/, "");
	    } else if (Schedule.drag.target) {
		var slots = Data.meetingSlots[Schedule.drag.object.id];
		var div = Schedule.drag.target;
		while (div && slots--) {
		    div.className = div.className.replace(/\boverhead\b/, "");

		    if (Data.slotNext[div.id]) {
			div = document.getElementById(Data.slotNext[div.id]);
		    } else {
			div = null;
		    }
		}
	    }

	    Schedule.drag.target = target;
	    if (Schedule.drag.target && Schedule.drag.target.id == 'dock') {
		Schedule.drag.target.className += " overhead";
	    } else if (Schedule.drag.target) {
		var slots = Data.meetingSlots[Schedule.drag.object.id];
		var div = Schedule.drag.target;
		while (div && slots--) {
		    div.className += " overhead";

		    if (Data.slotNext[div.id]) {
			div = document.getElementById(Data.slotNext[div.id]);
		    } else {
			div = null;
		    }
		}

		// Check as we pass over
		Schedule.save(true);
	    }
	}
    },

    // Ending a drag, if we dragged over a target then place the object in
    // the slot or dock and save the change - otherwise reset the drag
    mouseUp: function(ev) {
	if (! Schedule.drag)
	    return;

	// Always include a mouse move to keep the drag target up to date
	Schedule.mouseMove(ev);

	if (Schedule.drag.target) {
	    if (Schedule.drag.target.id == 'dock') {
		Schedule.drag.target.className = Schedule.drag.target.className.replace(/\boverhead\b/, "");
	    } else {
		var slots = Data.meetingSlots[Schedule.drag.object.id];
		var div = Schedule.drag.target;
		while (div && slots--) {
		    div.className = div.className.replace(/\boverhead\b/, "");

		    if (Data.slotNext[div.id]) {
			div = document.getElementById(Data.slotNext[div.id]);
		    } else {
			div = null;
		    }
		}
	    }

	    if (Schedule.drag.target.id == 'dock') {
		document.body.removeChild(Schedule.drag.object);
		Schedule.drag.target.appendChild(Schedule.drag.object);

		Schedule.drag.object.style.position = 'static';
		Schedule.drag.object.style.left     = null;
		Schedule.drag.object.style.top      = null;
		Schedule.drag.object.style.zIndex   = 'inherit';

		Schedule.save();
	    } else {
		document.body.removeChild(Schedule.drag.object);
		Schedule.drag.target.parentNode.insertBefore(Schedule.drag.object,
							     Schedule.drag.target);

		// Work out how high this needs to be to replace all the slots
		var slots = Data.meetingSlots[Schedule.drag.object.id];
		var div = Schedule.drag.target;
                var padding = (32 * (slots - 1))
                var height = parseInt(div.style.height) * slots + padding;

		Schedule.drag.object.style.position  = Schedule.drag.target.style.position;
		Schedule.drag.object.style.left      = Schedule.drag.target.style.left;
		Schedule.drag.object.style.top       = Schedule.drag.target.style.top;
		Schedule.drag.object.style.height    = height + 'px';
		Schedule.drag.object.style.minHeight = height + 'px';
		Schedule.drag.object.style.width     = Schedule.drag.target.style.width;
		Schedule.drag.object.style.zIndex    = Schedule.drag.target.style.zIndex;

		Schedule.save();
	    }

	} else {
	    Schedule.revert(Schedule.drag);
	}

	Schedule.drag = null;

	return false;
    },


    // Revert a drag, used in case of error or dragging into nowhere;
    // restore saved properties
    revert: function(drag) {
	drag.object.parentNode.removeChild(drag.object);
	drag.parentNode.insertBefore(drag.object, drag.nextSibling);

	drag.object.style.position  = drag.position;
	drag.object.style.left      = drag.left;
	drag.object.style.top       = drag.top;
	drag.object.style.height    = drag.height;
	drag.object.style.minHeight = drag.height;
	drag.object.style.width     = drag.width;
	drag.object.style.zIndex    = drag.zIndex;

	// Undo any error hilighting and reset the drag target so no async
	// command can take effect
	drag.target = null;
	Schedule.setError(drag.object, null, null);
    },

    // Save a change by constructing an AJAX POST back to the server,
    // this holds onto the current drag object
    save: function(check) {
	if (! (Schedule.drag && Schedule.drag.target))
	    return;

	var params = "meeting=" + Schedule.drag.object.id;
	if (Data.meeting[Schedule.drag.object.id])
	    params += "&remove=" + Data.meeting[Schedule.drag.object.id];
	if (Schedule.drag.target.id != 'dock')
	    params += "&add=" + Schedule.drag.target.id;

	if (check) {
	    params += "&check=y";

	    Ajax("POST", document.URL, Schedule.checkResponse,
		 Schedule.drag, params);
	} else {
	    Ajax("POST", document.URL, Schedule.saveResponse,
		 Schedule.drag, params);
	}
    },

    // Process a server response, this either updates our data about where
    // things are, or issues a revert on the saved drag
    saveResponse: function(elem, drag) {
	if (elem.tagName == "ok") {
	    var old_slot = Data.meeting[drag.object.id];
	    if (old_slot)
		Data.slot[old_slot] = null;

	    if (drag.target.id == 'dock') {
		Data.meeting[drag.object.id] = null;
	    } else {
		Data.meeting[drag.object.id] = drag.target.id;
		Data.slot[drag.target.id] = drag.object.id;
	    }

	    // Undo any error hilighting and reset the drag target so no async
	    // command can take effect
	    drag.target = null;
	    Schedule.setError(drag.object, null, elem);
	} else {
	    alert("Error: " + elem.textContent);

	    Schedule.revert(drag);
	}
    },

    // Process a server check response, we may update or clear the error list
    checkResponse: function(elem, drag) {
	if (! drag.target)
	    return;

	if ((elem.tagName != "ok") && elem.hasAttribute("target")
	    && (elem.getAttribute("target") == drag.target.id)) {
	    Schedule.setError(drag.object, elem.textContent, null);
	} else if (elem.tagName == "ok") {
	    Schedule.setError(drag.object, null, elem);
	} else {
	    Schedule.setError(drag.object, null, null);
	}
    },

    // Set an error message
    setError: function(obj, error, elem) {
	var errorspan = null;
	var titlespan = null;

	var spans = obj.getElementsByTagName('SPAN');
	for (var i = 0; i < spans.length; i++) {
	    var span = spans[i];

	    if (span.className == 'errortext')
		errorspan = span;
	    if (span.className == 'title')
		titlespan = span;
	}

	var warning = null;
	var missing = null;
	if (elem)
	    missing = elem.getElementsByTagName('missing');
	if (missing && missing.length)
	    warning = "Required participants unavailable";

	if (error) {
	    if (obj.className.match(/\bwarning\b$/))
		obj.className = obj.className.replace(/\bwarning\b/, "");
	    if (! obj.className.match(/\berror\b/))
		obj.className += " error";

	    if (errorspan)
		errorspan.parentNode.removeChild(errorspan);

	    errorspan = document.createElement('SPAN');
	    errorspan.className = 'errortext';
	    errorspan.appendChild(document.createTextNode(error));
	    obj.insertBefore(errorspan, titlespan.nextSibling);
	} else if (warning) {
	    if (obj.className.match(/\berror\b/))
		obj.className = obj.className.replace(/\berror\b/, "");
	    if (! obj.className.match(/\bwarning\b/))
		obj.className += " warning";

	    if (errorspan)
		errorspan.parentNode.removeChild(errorspan);

	    errorspan = document.createElement('SPAN');
	    errorspan.className = 'errortext';
	    errorspan.appendChild(document.createTextNode(warning));
	    obj.insertBefore(errorspan, titlespan.nextSibling);
	} else {
	    if (obj.className.match(/\berror\b/))
		obj.className = obj.className.replace(/\berror\b/, "");
	    if (obj.className.match(/\bwarning\b/))
		obj.className = obj.className.replace(/\bwarning\b/, "");
	    if (errorspan)
		errorspan.parentNode.removeChild(errorspan);
	}

	var participants = null;

    var uls = obj.getElementsByTagName('UL');
    for (var i = 0; i < uls.length; i++) {
        var ul = uls[i];

        if (ul.className == 'participants')
            participants = ul;
    }

    if (participants) {
        var links = participants.getElementsByTagName('A');
        for (var i = 0; i < links.length; i++) {
            var link = links[i];

            if (elem) {
            var ismissing = false;
            for (var m = 0; m < missing.length; m++) {
                if (link.textContent == missing[m].textContent)
                ismissing = true;
            }

            if (ismissing) {
                if (! link.parentNode.className.match(/\bmissing\b/))
                link.parentNode.className += " missing";
            } else {
                link.parentNode.className = link.parentNode.className.replace(/\bmissing\b/, "");
            }
            } else {
            link.parentNode.className = link.parentNode.className.replace(/\bmissing\b/, "");
            }
        }
    }
    },
};

window.addEventListener('load', Schedule.init, false);

# The Summit Scheduler web application
# Copyright (C) 2008 - 2013 Ubuntu Community, Canonical Ltd
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime
import pytz

from django.db.models import Q
from django.contrib.auth import logout
from django.http import Http404, HttpResponse, HttpResponseRedirect, HttpResponsePermanentRedirect
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.core.urlresolvers import reverse
from django.utils.datastructures import SortedDict
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required

from summit.schedule.decorators import (
    summit_required,
    summit_only_required,
    summit_attendee_required
)

from summit.schedule.models import (
    Summit,
    Slot,
    Attendee,
    Meeting,
    EventType,
    Track,
    Room,
    Participant,
    SummitSprint,
)
from summit.debconf_website.models import (
    UserProfile
)
from summit.schedule.render import schedule_factory, Schedule

from summit.schedule.forms import (
    CreateMeeting,
    OrganizerEditMeeting,
    ProposeMeeting,
    EditMeeting,
    MeetingReview,
    AttendMeeting,
    OrganizerChangeAttend,
    EditMeetingHangout,
    Registration,
)

__all__ = (
    'summit',
    'by_date',
    'by_room',
    'csv',
    'today_view',
)


@summit_required
def summit(request, summit, attendee):
    edit = False
    if (
        summit.state != 'public' and 'readonly' not in request.GET
    ) or 'edit' in request.GET:
        if request.user.is_authenticated() and summit.is_organizer(attendee):
            edit = True
    tracks = summit.track_set.all()

    lp_link = SummitSprint.objects.filter(summit=summit).exists()

    context = {
        'lp_link': lp_link,
        'summit': summit,
        'attendee': attendee,
        'edit': edit,
        'tracks': tracks,
        'summit_organizer': summit.is_organizer(attendee),
    }
    return render_to_response("schedule/summit.html", context,
                              context_instance=RequestContext(request))


@summit_required
def search(request, summit, attendee):
    query = request.GET.get('q', None)
    meetings = []
    if query:
        matches = summit.meeting_set.filter(
            Q(name__icontains=query) | Q(title__icontains=query)
        )
        for m in matches:
            if not m.private or m.can_view_pm(attendee):
                meetings.append(m)
    context = {
        'summit': summit,
        'query': query,
        'meetings': meetings,
    }
    return render_to_response("schedule/search.html", context,
                              context_instance=RequestContext(request))


@summit_required
def daily_schedule(request, summit, attendee, date):
    viewdate = summit.as_localtime(
        datetime.datetime.strptime(date, "%Y-%m-%d")
    )
    utc_date = summit.delocalize(viewdate)
    day = datetime.timedelta(days=1)

    schedule = SortedDict()
    multislot_meetings = SortedDict()

    for slot in summit.slot_set.filter(
        start_utc__gte=utc_date,
        end_utc__lte=(utc_date+day)
    ).order_by('start_utc'):
        if not (
            slot.type == 'open' or
            slot.type == 'plenary' or
            slot.type == 'lunch'
        ):
            continue
        if not slot in schedule:
            schedule[slot] = SortedDict()

        # Start with multi-slot meetings carried over from previous slots
        for agenda, count in multislot_meetings.items():
            schedule[slot][agenda.room] = agenda
            if count == 1:
                del multislot_meetings[agenda]
            else:
                multislot_meetings[agenda] = count - 1

        # Add meetings from this slot
        for agenda in slot.agenda_set.select_related().order_by('room__name'):
            if not agenda.meeting.private or attendee in agenda.meeting.attendees:
                schedule[slot][agenda.room] = agenda
                if agenda.meeting.slots > 1:
                    multislot_meetings[agenda] = agenda.meeting.slots - 1

    if '_popup' in request.GET:
        is_popup = True

    context = {
        'summit': summit,
        'attendee': attendee,
        'schedule': schedule,
        'ical': '/%s.ical' % summit.name,
        'viewdate': viewdate,
        'nextday': viewdate + day,
        'previousday': viewdate - day,
        'summit_organizer': summit.is_organizer(attendee),
    }
    return render_to_response("schedule/daily.html", context,
                              context_instance=RequestContext(request))

dc_room_order = {
 'debconf14': (4,1,2,3,5,6,7,8, None),
 'debconf15': (10,11,12,13,14,15,16,9, None),
}
dc_rooms = {
 'debconf14': ('room-327-8-9', 'room-327', 'room-328', 'room-329', 'elsewehere', 'frontdesk', 'elsewhere2', 'ballroom', 'cafeteria'), 
 'debconf15': ("Heidelberg", "Berlin/London", "Amsterdam", "Helsinki", "Stockholm", "Madrid", "Wien", "elsewhere", "cafeteria"),
}

@summit_only_required
def grid_date_plain(request, summit, date):
    day = summit.as_localtime(
        datetime.datetime.strptime(date, "%Y-%m-%d")
    )
    return _grid_schedule(request, summit, [day], "schedule/plain_grid.html")

@summit_only_required
def grid_schedule(request, summit):
    return _grid_schedule(request, summit, summit.days())

@summit_only_required
def grid_date(request, summit, date):
    day = summit.as_localtime(
        datetime.datetime.strptime(date, "%Y-%m-%d")
    )
    return _grid_schedule(request, summit, [day])

@summit_only_required
def grid_today(request, summit):
    day = datetime.datetime.now(pytz.timezone(summit.timezone)).replace(hour=0, minute=0, second=0, microsecond=0)
    first_day = summit.days()[0]
    if day < first_day:
        day = first_day
    return _grid_schedule(request, summit, [day])

def _grid_schedule(request, summit, selected_days, template=None):
    edition_name = summit.name
    rooms = dc_rooms[edition_name]
    room_order = dc_room_order[edition_name]
    days = SortedDict()
    empty_slot = {'type': "none"}
    for day in selected_days:
        day_str = day.strftime( "%A, %Y-%m-%d")
        rows = SortedDict()
        start_day = None
        end_day = (0, 0)

        slots = summit.slot_set.filter(
            start_utc__gte=day,
            start_utc__lte=(day + datetime.timedelta(days=1))
        ).order_by('start_utc')

        if not len(slots):
            continue

        for slot in slots:
            start = summit.localize(slot.start_utc)
            end = summit.localize(slot.end_utc)
            start_time = (start.hour, start.minute - start.minute % 5)
            end_time = (end.hour, end.minute - end.minute % 5)
            if end >= (day + datetime.timedelta(days=1)) and end.hour < 5:
                end_time = (end_time[0] + 24, end_time[1])
            if not start_day:
                start_day = start_time
                times = [(h,m) for h in range(start_day[0], 30) for m in range(0, 60, 5)] + [(99, 99)]
                for time in times:
                    rows[time] = { "slot": empty_slot,
                                   "duration": 1,
                                   "list": [None]*len(rooms),
                                 }
            end_day = max(end_time, end_day)
            used_rows = []
            h,m = start_time
            duration = 0
            while True:
                used_rows.append((h,m))
                m += 5
                duration += 1
                if m >= 60:
                    h += 1
                    m = 0
                if not (h < end_time[0] or (h == end_time[0] and m < end_time[1])):
                    break
            rows[start_time]["slot"] = slot
            rows[start_time]["duration"] = duration

            if slot.type == 'lunch':
                meal = "Break"
                # we do some extra check to distinguish ev. coffee break, but still having generic times
                if end_time < (11,0) and start_time < (9,0) and duration > 10:
                    meal = "Breakfast"
                elif end_time < (14,30) and start_time > (11,0) and duration > 16:
                    meal = "Lunch"
                elif start_time > (17,0) and duration > 16:
                    meal = "Dinner"
                elif duration >= 3:
                    meal = "Coffee Break"
                rows[start_time]['list'][-1] = {
                        'meal': meal, 'duration': duration }
                if len(used_rows) > 1:
                    for t in used_rows[1:]:
                        rows[t]['list'][-1] = "ignore"

            # Add meetings from this slot
            for agenda in slot.agenda_set.select_related():
                if not agenda.meeting.private:
                    names = []
                    for speaker in agenda.meeting.speakers.all():
                        userprofile = UserProfile.objects.get(id=speaker.id)
                        names.append(userprofile.badge_full)
                    try:
                        room = room_order.index(agenda.room.id)
                    except ValueError:
                        room = len(room_order)-2
                    rows[start_time]['list'][room] = {
                        'talk': agenda.meeting,
                        'speakers': ", ".join(names),
                        'duration': duration,
                    }
                    colors = agenda.meeting.tracks.values_list('color', flat=True)
                    for value in colors:
                        # Skip white backgrounds
                        if value and value.upper() != 'FFFFFF':
                            color = value
                            break
                    else:
                        color = agenda.meeting.eventtype.color
                    rows[start_time]['list'][room]['bgcolor'] = color
                    # merged row, so we need to skip the next (below) cells
                    for t in used_rows[1:]:
                        rows[t]['list'][room] = "ignore"

        # Compress the schedule
        skipable = 0
        previous_time = None
        for time in rows:
            for r in range(len(rooms)-2):
                # long talks are not compressable (talks scheduled in talk rooms)
                if rows[time]['list'][r]:
                    skipable = 0
                    previous_time = time
                    continue
            for r in range(len(rooms)-2, len(rooms)):
                # special events and meals: compress on long ignore
                if rows[time]['list'][r] and rows[time]['list'][r] != "ignore":
                    skipable = 0
                    previous_time = time
                    continue
                # we need to publish in any case the end time [really, start of next slot]
                if previous_time and not rows[time]['list'][r] and rows[previous_time]['list'][r]:
                    skipable = 0
                    previous_time = time
                    continue
            skipable += 1
            previous_time = time
            if skipable < 3:
                continue
            # now we can compress
            rows[time]['hidden'] = True

        # Add last slot
        if end_day:
            slot = {'type': "end"}
            rows[end_day] = {"slot": slot, "list": [None]*len(rooms)}
            for t in times[times.index(end_day)+1:]:
                del rows[t]

        days[day_str] = { 'rooms': rooms, 'rows': rows, 'date': day,
                          'nextday': day + datetime.timedelta(days=1),
                          'previousday': day - datetime.timedelta(days=1)
                        }

    context = {
        'summit': summit,
        'days': days,
        'section': "DebConf"
    }
    if not template:
        template = "schedule/grids.html"
    return render_to_response(template, context,
                              context_instance=RequestContext(request))


@summit_attendee_required
def attendee_schedule(request, summit, attendee):
    pass


@csrf_exempt
@summit_required
def by_date(request, summit, attendee, date):
    return _process_date_view(request, summit, attendee, date)


@summit_required
def today_view(request, summit, attendee):
    today = summit.localize(datetime.datetime.now()).date()
    return _process_date_view(
        request,
        summit,
        attendee,
        today.strftime("%Y-%m-%d")
    )


def _process_date_view(request, summit, attendee, date):
    if 'rooms' in request.GET:
        roomnames = request.GET.get('rooms', '').split(',')
        rooms = list(summit.room_set.filter(name__in=roomnames, type='open'))
        if request.user.is_authenticated() and request.user.is_staff:
            rooms += list(
                summit.room_set.filter(
                    name__in=roomnames,
                    type='private'
                )
            )
    else:
        rooms = None
    schedule = schedule_factory(
        request,
        summit,
        attendee,
        room=rooms,
        date=date
    )

    if request.method == 'POST':
        return schedule.save_change()
    else:
        viewdate = datetime.datetime.strptime(date, "%Y-%m-%d")
        day = datetime.timedelta(days=1)

        context = {
            'summit': summit,
            'attendee': attendee,
            'schedule': schedule,
            'ical': '/%s.ical' % summit.name,
            'autoreload': 'reload' in request.GET,
            'monitor': 'monitor' in request.GET,
            'nextday': viewdate + day,
            'viewdate': viewdate,
            'previousday': viewdate - day,
            'can_change_agenda': summit.can_change_agenda(attendee),
        }
        converted_date = summit.delocalize(
            datetime.datetime.strptime(date, "%Y-%m-%d")
        )
        if 'edit' in request.GET or Slot.objects.filter(
            summit=summit,
            start_utc__gte=converted_date,
            end_utc__lte=converted_date+datetime.timedelta(days=1)
        ).count() > 0:
            schedule.calculate()
        else:
            return render_to_response("schedule/nosession.html", context,
                                      context_instance=RequestContext(request))
    return render_to_response("schedule/schedule.html", context,
                              context_instance=RequestContext(request))


@summit_required
def next_table(request, summit, attendee):
    if 'rooms' in request.GET:
        roomnames = request.GET.get('rooms', '').split(',')
        rooms = list(summit.room_set.filter(name__in=roomnames, type='open'))
        if request.user.is_authenticated() and request.user.is_staff:
            rooms += list(
                summit.room_set.filter(
                    name__in=roomnames, type='private'
                )
            )
    else:
        rooms = None
    schedule = Schedule.from_request(
        request,
        summit,
        attendee,
        room=rooms,
        nextonly=True
    )

    schedule.calculate()

    context = {
        'summit': summit,
        'attendee': attendee,
        'schedule': schedule,
        'autoreload': 'reload' in request.GET,
    }
    return render_to_response("schedule/nextsession_table.html", context,
                              context_instance=RequestContext(request))


@summit_required
def next_session(request, summit, attendee):
    url_content = request.GET.urlencode(safe='_:')

    context = {
        'summit': summit,
        'autoreload': 'reload' in request.GET,
        'url_content': url_content
    }
    return render_to_response("schedule/nextsession.html", context,
                              context_instance=RequestContext(request))


@summit_required
def by_room(request, summit, attendee, room_name):
    schedule = SortedDict()
    multislot_meetings = SortedDict()

    room = get_object_or_404(Room, name=room_name, summit=summit)

    for slot in summit.slot_set.all().order_by('start_utc'):

        if not (
            slot.type == 'open' or
            slot.type == 'plenary' or
            slot.type == 'lunch'
        ):
            continue
        if not slot in schedule:
            schedule[slot] = SortedDict()

        # Start with multi-slot meetings carried over from previous slots
        for agenda, count in multislot_meetings.items():
            schedule[slot][agenda.room] = agenda
            if count == 1:
                del multislot_meetings[agenda]
            else:
                multislot_meetings[agenda] = count - 1

        # Add meetings from this slot
        for agenda in slot.agenda_set.filter(room=room).select_related():
            if not agenda.meeting.private or attendee in agenda.meeting.attendees:
                schedule[slot][agenda.room] = agenda
                if agenda.meeting.slots > 1:
                    multislot_meetings[agenda] = agenda.meeting.slots - 1
        if len(schedule[slot]) < 1:
            del schedule[slot]

    context = {
        'room': room,
        'summit': summit,
        'schedule': schedule,
        'attendee': attendee,
        'summit_organizer': summit.is_organizer(attendee),
    }
    return render_to_response("schedule/by_room.html", context,
                              context_instance=RequestContext(request))


@summit_required
def by_track(request, summit, attendee, track_slug):
    schedule = SortedDict()
    multislot_meetings = SortedDict()

    track = get_object_or_404(Track, slug=track_slug, summit=summit)

    for slot in summit.slot_set.all().order_by('start_utc'):

        if not (
            slot.type == 'open' or
            slot.type == 'plenary' or
            slot.type == 'lunch'
        ):
            continue
        if not slot in schedule:
            schedule[slot] = SortedDict()

        # Start with multi-slot meetings carried over from previous slots
        for agenda, count in multislot_meetings.items():
            schedule[slot][agenda.room] = agenda
            if count == 1:
                del multislot_meetings[agenda]
            else:
                multislot_meetings[agenda] = count - 1

        # Add meetings from this slot
        for agenda in slot.agenda_set.filter(
            meeting__tracks=track
        ).select_related().order_by('room__name'):
            if not agenda.meeting.private or attendee in agenda.meeting.attendees:
                schedule[slot][agenda.room] = agenda
                if agenda.meeting.slots > 1:
                    multislot_meetings[agenda] = agenda.meeting.slots - 1
        if len(schedule[slot]) < 1:
            del schedule[slot]

    context = {
        'track': track,
        'summit': summit,
        'schedule': schedule,
        'attendee': attendee,
        'summit_organizer': summit.is_organizer(attendee),
    }
    return render_to_response("schedule/by_track.html", context,
                              context_instance=RequestContext(request))


@summit_attendee_required
def by_participant(request, summit, attendee, username):
    schedule = SortedDict()
    multislot_meetings = SortedDict()

    participant = get_object_or_404(
        Attendee,
        user__username=username,
        summit=summit
    )

    for slot in summit.slot_set.all().order_by('start_utc'):

        if not (
            slot.type == 'open' or
            slot.type == 'plenary' or
            slot.type == 'lunch'
        ):
            continue
        if not slot in schedule:
            schedule[slot] = SortedDict()

        # Start with multi-slot meetings carried over from previous slots
        for agenda, count in multislot_meetings.items():
            schedule[slot][agenda.room] = agenda
            if count == 1:
                del multislot_meetings[agenda]
            else:
                multislot_meetings[agenda] = count - 1

        # Add meetings from this slot
        for agenda in slot.agenda_set.filter(
            Q(
                meeting__participant__attendee=participant
            ) | Q(
                meeting__drafter=participant
            ) | Q(
                meeting__assignee=participant
            )
        ).select_related().order_by('room__name'):
            if not agenda.meeting.private or attendee in agenda.meeting.attendees:
                schedule[slot][agenda.room] = agenda
                if agenda.meeting.slots > 1:
                    multislot_meetings[agenda] = agenda.meeting.slots - 1
        if len(schedule[slot]) < 1:
            del schedule[slot]

    context = {
        'participant': participant,
        'summit': summit,
        'schedule': schedule,
        'attendee': attendee,
        'summit_organizer': summit.is_organizer(attendee),
    }
    return render_to_response("schedule/by_participant.html", context,
                              context_instance=RequestContext(request))


@summit_only_required
def tracks(request, summit):

    context = {
        'summit': summit,
    }
    return render_to_response("schedule/tracks.html", context,
                              context_instance=RequestContext(request))


@summit_required
def meeting(request, summit, attendee, meeting_id, meeting_slug):
    meeting = get_object_or_404(summit.meeting_set, id=meeting_id)
    # Canonicalise
    if meeting_slug != meeting.name:
        return HttpResponsePermanentRedirect(meeting.get_meeting_page_url())
    if meeting.private and not meeting.can_view_pm(attendee):
        raise Http404
    return _show_meeting(request, summit, meeting, attendee)


@summit_required
def private_meeting(request, summit, attendee, private_key, meeting_slug):
    meeting = get_object_or_404(summit.meeting_set, private_key=private_key)
    return _show_meeting(request, summit, meeting, attendee)


def _show_meeting(request, summit, meeting, attendee):
    agendaitems = meeting.agenda_set.all()
    participants = meeting.participant_set.all()
    speakers = meeting.speakers.all()
    attendees = meeting.attendees
    required_attendees = meeting.required_attendees
    tracks = meeting.tracks.all()
    user_is_attending = attendee is not None and attendee in meeting.attendees
    user_is_participating = attendee is not None and attendee in [p.attendee for p in participants if not p.from_launchpad]

    if attendee is not None and attendee in meeting.required_attendees:
        required = True
    else:
	required = False

    if attendee is not None and attendee == meeting.drafter:
        drafter = True
    else:
        drafter = False

    if attendee is not None and attendee in speakers:
	speaker = True
    else:
	speaker = False

    context = {
        'summit': summit,
        'meeting': meeting,
        'attendee': attendee,
        'agenda_items': agendaitems,
        'speakers': speakers,
        'participants': participants,
        'attendees': attendees,
        'required_attendees': required_attendees,
        'user_is_attending': user_is_attending,
        'user_is_participating': user_is_participating,
        'required': required,
        'tracks': tracks,
        'ETHERPAD_HOST': summit.etherpad,
        'summit_organizer': summit.is_organizer(attendee),
        'scheduler': summit.can_change_agenda(attendee),
        'is_scheduler': summit.is_scheduler(attendee),
        'speaker': speaker,
        'drafter': drafter,
    }
    if summit.virtual_summit or meeting.virtual_meeting:
        return render_to_response(
            "schedule/virtual_meeting.html",
            context,
            context_instance=RequestContext(request)
        )
    else:
        return render_to_response(
            "schedule/meeting.html",
            context,
            context_instance=RequestContext(request)
        )


@summit_required
def share_meeting(request, summit, attendee, meeting_id, meeting_slug):
    meeting = get_object_or_404(summit.meeting_set, id=meeting_id)
    if meeting.private:
        # check if the user should be able to see this private meeting
        if attendee is None:
            raise Http404

        attendee_allowed = False
        for a in meeting.attendees:
            if a.user.pk == attendee.user.pk:
                meeting.share()
                return HttpResponseRedirect(
                    reverse(
                        private_meeting,
                        args=[summit.name, meeting.private_key, meeting_slug]
                    )
                )
        raise Http404
    return HttpResponseRedirect(
        reverse(
            meeting,
            summit.name,
            meeting.id,
            meeting_slug
        )
    )


@summit_attendee_required
def register(request, summit, attendee, meeting_id, meeting_slug):
    meeting = get_object_or_404(summit.meeting_set, id=meeting_id)
    user_is_attending = attendee is not None and attendee in meeting.attendees
    if not user_is_attending:
        meeting.participant_set.create(
            attendee=attendee,
            participation='ATTENDING',
            from_launchpad=False
        )

    return HttpResponseRedirect(
        reverse(
            'summit.schedule.views.meeting',
            args=(summit.name, meeting.id, meeting_slug)
        )
    )


@summit_attendee_required
def unregister(request, summit, attendee, meeting_id, meeting_slug):
    meeting = get_object_or_404(summit.meeting_set, id=meeting_id)
    meeting.participant_set.filter(
        attendee=attendee,
        from_launchpad=False
    ).delete()

    return HttpResponseRedirect(
        reverse(
            'summit.schedule.views.meeting',
            args=(summit.name, meeting.id, meeting_slug)
        )
    )


@summit_only_required
def csv(request, summit):
    schedule = Schedule.from_request(request, summit)
    schedule.calculate()

    return HttpResponse(schedule.as_csv(),
                        mimetype='text/csv')


@summit_only_required
def ical(request, summit):
    """Return any list events as an ical"""
    schedule = Schedule.from_request(request, summit)
    schedule.calculate()

    filename = "%s.ical" % summit.name.replace(' ', '-').lower()
    response = HttpResponse(mimetype='text/calendar')
    response['Content-Disposition'] = 'attachment; filename=%s' % filename.encode('ascii', 'replace')
    response.write(schedule.as_ical())
    return response


@summit_attendee_required
def user_ical(request, summit, username):
    """Returns a user's registered events as an ical"""
    schedule = Schedule.from_request(request, summit)
    schedule.calculate()

    filename = "%s_%s.ical" % (summit.name, username)
    response = HttpResponse(mimetype='text/calendar')
    response['Content-Disposition'] = 'attachment; filename=%s' % filename.replace(' ', '-').lower().encode('ascii', 'replace')
    response.write(schedule.as_ical(only_username=username))
    return response


@summit_attendee_required
def user_private_ical(request, summit, attendee, secret_key):
    """Returns a user's registered events as an ical"""
    attendee = get_object_or_404(Attendee, summit=summit, secret_key_id=secret_key)
    schedule = Schedule.from_request(request, summit, show_private=True)
    schedule.calculate()

    response = HttpResponse(mimetype='text/calendar')
    response['Content-Disposition'] = 'attachment; filename=my_schedule_%s.ical' % attendee.secret_key
    response.write(
        schedule.as_ical(
            only_username=attendee.user.username,
            show_private=True
        )
    )
    return response


@summit_only_required
def room_ical(request, summit, room_name):
    """Returns a room's events as an ical"""
    schedule = Schedule.from_request(request, summit)
    schedule.calculate()

    filename = "%s_%s.ical" % (summit.name, room_name)
    response = HttpResponse(mimetype='text/calendar')
    response['Content-Disposition'] = 'attachment; filename=%s' % filename.replace(' ', '-').lower().encode('ascii', 'replace')
    response.write(schedule.as_ical(only_room=room_name))
    return response


@summit_only_required
def track_ical(request, summit, track_slug):
    """Returns a track's events as an ical"""
    schedule = Schedule.from_request(request, summit)
    schedule.calculate()

    filename = "%s_%s.ical" % (summit.name, track_slug)
    response = HttpResponse(mimetype='text/calendar')
    response['Content-Disposition'] = 'attachment; filename=%s' % filename.replace(' ', '-').lower().encode('ascii', 'replace')
    response.write(schedule.as_ical(only_track=track_slug))
    return response


@summit_only_required
def xml(request, summit):
    """Return any list events as an pentabarf xml"""
    schedule = Schedule.from_request(request, summit)
    schedule.calculate()

    filename = "%s.xml" % summit.name.replace(' ', '-').lower()
    response = HttpResponse(mimetype='application/xml')
    response.write(schedule.as_xml())
    return response


def logout_view(request):
    logout(request)
    return HttpResponseRedirect('/')


@summit_required
def mobile(request, summit, attendee):
    context = {
        'summit': summit,
        'attendee': attendee,
    }
    return render_to_response("schedule/mobile.html", context, RequestContext(request))


def past(request):
    pastsummit = Summit.on_site.filter(date_end__lte=datetime.date.today())
    context = {
        'past_summit': pastsummit,
    }
    return render_to_response(
        "schedule/past_summit.html",
        context,
        context_instance=RequestContext(request)
    )


@summit_attendee_required
def create_meeting(request, summit, attendee):

    if not summit.is_organizer(attendee):
        return HttpResponseRedirect(
            reverse(
                'summit.schedule.views.summit',
                args=(summit.name,)
            )
        )
    else:
        meeting = Meeting(
            summit=summit,
            approver=attendee,
            drafter=attendee,
            approved='APPROVED'
        )

        if request.method == 'POST':
            form = CreateMeeting(data=request.POST, instance=meeting)
            if form.is_valid():
                form.save()
                return HttpResponseRedirect(meeting.meeting_page_url)
        else:
            form = CreateMeeting(instance=meeting)

    context = {
        'summit': summit,
        'form': form,
    }
    return render_to_response(
        'schedule/create_meeting.html',
        context,
        RequestContext(request)
    )


@login_required
@summit_required
def propose_meeting(request, summit, attendee):
    if summit == None or summit.state == "archived":
        return render_to_response('errormsg.html', {
                'summit': summit,
                'errormsg': "Call for Paper for %s is closed" % summit.title},
            RequestContext(request))
    elif summit.state == "setup" and (attendee == None or attendee.user not in summit.managers.all()):
        return render_to_response('errormsg.html', {
                'summit': summit,
                'errormsg': "Call for Paper for %s is not yet open for public" % summit.title},
            RequestContext(request))

    eod=datetime.time(23, 59, 59)
    eos=summit.date_end
    if attendee is None:
        attendee = UserProfile.objects.create(
            summit=summit,
            user=request.user,
	    attend=False,
            start_utc=summit.date_start,
            end_utc=datetime.datetime.combine(eos, eod)
        )

    meeting = Meeting(
        summit=summit,
        drafter=attendee,
        private=False,
        approved='PENDING'
    )

    if request.method == 'POST':
        form = ProposeMeeting(data=request.POST, instance=meeting)
        if form.is_valid():
            new_meeting = form.save()
            new_meeting.speakers.add(attendee)
            return HttpResponseRedirect(meeting.meeting_page_url)
    else:
        form = ProposeMeeting(instance=meeting)

    context = {
        'summit': summit,
        'form': form,
    }
    return render_to_response(
        'schedule/propose_meeting.html',
        context,
        RequestContext(request)
    )


@summit_attendee_required
def organizer_edit_meeting(
    request,
    summit,
    attendee,
    meeting_id,
    meeting_slug
):
    meeting = get_object_or_404(summit.meeting_set, id=meeting_id)

    if not summit.is_organizer(attendee):
        return HttpResponseRedirect(
            reverse(
                'summit.schedule.views.summit',
                args=(summit.name,)
            )
        )
    else:
        if request.method == 'POST':
            form = OrganizerEditMeeting(data=request.POST, instance=meeting)
            if form.is_valid():
                form.save()
                return HttpResponseRedirect(meeting.meeting_page_url)
        else:
            form = OrganizerEditMeeting(instance=meeting)

    context = {
        'summit': summit,
        'form': form,
    }
    return render_to_response(
        'schedule/org_edit_meeting.html',
        context,
        RequestContext(request)
    )


@summit_attendee_required
def edit_meeting(request, summit, attendee, meeting_id, meeting_slug):
    meeting = get_object_or_404(summit.meeting_set, id=meeting_id)

    if attendee != meeting.drafter and attendee not in meeting.speakers_set.all():
        return HttpResponseRedirect(
            reverse(
                'summit.schedule.views.summit',
                args=(summit.name,)
            )
        )
    else:
        if request.method == 'POST':
            form = EditMeeting(data=request.POST, instance=meeting)
            if form.is_valid():
                form.save()
                return HttpResponseRedirect(meeting.meeting_page_url)
        else:
            form = EditMeeting(instance=meeting)

    context = {
        'summit': summit,
        'form': form,
    }
    return render_to_response(
        'schedule/edit_meeting.html',
        context,
        RequestContext(request)
    )


@summit_attendee_required
def edit_meeting_hangout(
    request,
    summit,
    attendee,
    meeting_id,
    meeting_slug
):
    meeting = get_object_or_404(summit.meeting_set, id=meeting_id)
    # TODO: Add logic that prevents this view from being accessed if the
    # Summit or meeting is not virtual
    if not summit.is_organizer(attendee) and attendee != meeting.drafter and attendee not in meeting.speakers_set.all():
        return HttpResponseRedirect(
            reverse(
                'summit.schedule.views.summit',
                args=(summit.name,)
            )
        )
    else:
        if request.method == 'POST':
            form = EditMeetingHangout(data=request.POST, instance=meeting)
            if form.is_valid():
                form.save()
                return HttpResponseRedirect(meeting.meeting_page_url)
        else:
            form = EditMeetingHangout(instance=meeting)

    context = {
        'summit': summit,
        'form': form,
    }
    return render_to_response(
        'schedule/edit_hangout.html',
        context,
        RequestContext(request)
    )


@summit_required
def review_pending(request, summit, attendee):

    if not summit.is_organizer(attendee):
        return HttpResponseRedirect(
            reverse(
                'summit.schedule.views.summit',
                args=(summit.name,)
            )
        )
    else:
        schedule = SortedDict()

        for track in summit.track_set.all():
            if not track in schedule:
                schedule[track] = list()

            # Add meetings from this slot
            for meeting in track.meeting_set.exclude(approved='APPROVED'):
                schedule[track].append(meeting)

    context = {
        'summit': summit,
        'schedule': schedule,
    }
    return render_to_response("schedule/review.html", context,
                              context_instance=RequestContext(request))


@summit_required
def meeting_review(request, summit, attendee, meeting_id):
    meeting = get_object_or_404(summit.meeting_set, id=meeting_id)

    if not summit.is_organizer(attendee):
        return HttpResponseRedirect(
            reverse(
                'summit.schedule.views.summit',
                args=(summit.name,)
            )
        )
    else:
        if request.method == 'POST':
            form = MeetingReview(data=request.POST, instance=meeting)
            if form.is_valid():
                meeting = form.save(commit=False)
                meeting.approver = attendee
                meeting.save()
                return HttpResponseRedirect(meeting.meeting_page_url)
        else:
            form = MeetingReview(instance=meeting)

    context = {
        'summit': summit,
        'form': form,
    }
    return render_to_response(
        'schedule/meeting_review.html',
        context,
        RequestContext(request)
    )


@summit_only_required
def all_meetings(request, summit):

    schedule = SortedDict()

    for track in summit.track_set.all():
        if not track in schedule:
            schedule[track] = list()

        # Add meetings from this slot
        for meeting in track.meeting_set.filter(approved='APPROVED'):
            schedule[track].append(meeting)

    context = {
        'summit': summit,
        'schedule': schedule,
    }
    return render_to_response(
        "schedule/all_meetings.html",
        context,
        context_instance=RequestContext(request),
    )


@summit_required
def created_meetings(request, summit, attendee, username):

    drafter = get_object_or_404(
        Attendee,
        summit=summit,
        user__username=username
    )
    if attendee is None or attendee.id != drafter.id:
        meetings = summit.meeting_set.filter(
            drafter=drafter
        ).exclude(private=True)
    else:
        meetings = summit.meeting_set.filter(drafter=drafter)

    context = {
        'summit': summit,
        'meetings': meetings,
        'drafter': drafter,
        'attendee': attendee,
    }
    return render_to_response(
        "schedule/mine.html",
        context,
        context_instance=RequestContext(request)
    )


@summit_required
def meeting_copy(request, summit, attendee, meeting_id, meeting_slug):
    meeting = get_object_or_404(summit.meeting_set, id=meeting_id)

    if not summit.can_change_agenda(attendee):
        return HttpResponseRedirect(
            reverse(
                'summit.schedule.views.summit',
                args=(summit.name,)
            )
        )
    else:
        if request.method == 'POST':
            form = CreateMeeting(instance=meeting, data=request.POST)
            meeting.id = meeting.pk = None
            meeting.approver = attendee
            meeting.drafter = attendee
            meeting.approved = 'APPROVED'

            if form.is_valid():
                meeting = form.save()
                meeting_id = meeting.id
                return HttpResponseRedirect(meeting.meeting_page_url)
        else:
            form = CreateMeeting(instance=meeting)

    context = {
        'summit': summit,
        'form': form,
    }

    return render_to_response('schedule/create_meeting.html',
                          context, RequestContext(request))


@summit_attendee_required
def attend_meeting(request, summit, attendee, meeting_id):
    meeting = get_object_or_404(summit.meeting_set, id=meeting_id)

    try:
        participant = Participant.objects.get(
            attendee=attendee,
            meeting=meeting
        )
    except Participant.DoesNotExist:
        participant = Participant(attendee=attendee, meeting=meeting)

    if request.method == 'POST':
        form = AttendMeeting(data=request.POST, instance=participant)
        if form.is_valid():
            form.save()

            return HttpResponseRedirect(meeting.meeting_page_url)
    else:
        form = AttendMeeting(instance=participant)

    context = {
        'summit': summit,
        'form': form,
        'meeting.title': meeting.title,
        'meeting': meeting,
    }
    return render_to_response(
        'schedule/attend.html',
        context,
        RequestContext(request)
    )


@summit_required
def attendee_review(request, summit, attendee, meeting_slug, meeting_id):
    meeting = get_object_or_404(summit.meeting_set, id=meeting_id)

    if not summit.is_organizer(attendee) and attendee != meeting.drafter and attendee not in speakers_set.all():
        return HttpResponseRedirect(
            reverse(
                'summit.schedule.views.summit',
                args=(summit.name,)
            )
        )
    else:
        participants = meeting.participant_set.all()

    context = {
        'summit': summit,
        'meeting': meeting,
        'participants': participants,
        'summit_organizer': summit.is_organizer(attendee),

    }
    return render_to_response("schedule/attendees.html", context,
                              context_instance=RequestContext(request))


@summit_attendee_required
def organizer_edit_attendees(
    request,
    summit,
    logged_in_user,
    username,
    meeting_id
):
    meeting = get_object_or_404(summit.meeting_set, id=meeting_id)
    attendee = get_object_or_404(
        Attendee,
        summit=summit,
        user__username=username
    )
    meeting_slug = get_object_or_404(Meeting, summit=summit, id=meeting_id)

    if not summit.is_organizer(
        logged_in_user
    ) and logged_in_user != meeting.drafter:
        return HttpResponseRedirect(
            reverse(
                'summit.schedule.views.summit',
                args=(summit.name,)
            )
        )
    else:
        try:
            participant = Participant.objects.get(
                attendee=attendee,
                meeting=meeting
            )
        except Participant.DoesNotExist:
            participant = Participant(attendee=attendee, meeting=meeting)

        if request.method == 'POST':
            form = OrganizerChangeAttend(
                data=request.POST,
                instance=participant
            )
            if form.is_valid():
                form.save()
                return HttpResponseRedirect(
                    reverse(
                        'summit.schedule.views.attendee_review',
                        args=(summit.name, meeting.id, meeting.name)
                    )
                )
        else:
            form = OrganizerChangeAttend(instance=participant)

    context = {
        'summit': summit,
        'form': form,
        'meeting.title': meeting.title,
        'meeting': meeting,
        'attendee': attendee,
    }
    return render_to_response(
        'schedule/org_attend.html',
        context,
        RequestContext(request)
    )


@summit_attendee_required
def delete_meeting_confirm(
    request,
    summit,
    attendee,
    meeting_id,
    meeting_slug
):
    meeting = get_object_or_404(summit.meeting_set, id=meeting_id)

    if not attendee.user in summit.schedulers.all() and not attendee.user.has_perm('schedule.change_agenda'):
        return HttpResponseRedirect(
            reverse(
                'summit.schedule.views.summit',
                args=(summit.name,)
            )
        )

    context = {
        'summit': summit,
        'meeting.title': meeting.title,
        'meeting': meeting,
    }
    return render_to_response(
        'schedule/delete_confirm.html',
        context,
        RequestContext(request)
    )


@summit_attendee_required
def delete_meeting(request, summit, attendee, meeting_id, meeting_slug):
    meeting = get_object_or_404(summit.meeting_set, pk=meeting_id)
    if not attendee.user in summit.schedulers.all() and not attendee.user.has_perm('schedule.change_agenda'):
        return HttpResponseRedirect(
            reverse(
                'summit.schedule.views.summit',
                args=(summit.name,)
            )
        )
    else:
        meeting.approved = 'REMOVED'
        meeting.save()
        meeting.agenda_set.all().delete()
        return HttpResponseRedirect(
            reverse(
                'summit.schedule.views.delete_confirmed',
                args=(summit.name,)
            )
        )


def delete_confirmed(request, summit_name):
    summit = get_object_or_404(Summit, name=summit_name)

    context = {
        'summit': summit
    }

    return render_to_response(
        'schedule/confirm_deletion.html',
        context,
        RequestContext(request)
    )


@login_required
@summit_required
def registration_form(request, summit, attendee):
    registration_args = dict()

    if attendee is None:
        attendee = Attendee(
            user=request.user,
            summit=summit,
            from_launchpad=False,
            start_utc=summit.start or summit.date_start,
            end_utc=summit.end or summit.date_end,
        )
        registration_args['instance'] = attendee
        form_title="Register for %s" % summit.title
    else:
        registration_args['instance'] = attendee
        form_title="Update registration for %s" % summit.title

    if request.method == 'POST':
        form = Registration(data=request.POST, **registration_args)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(
                reverse(
                    'summit.schedule.views.summit',
                    args=(summit.name,)
                )
            )
    else:
        form = Registration(**registration_args)

    context = {
        'summit': summit,
        'form': form,
        'form_title': form_title,
    }
    return render_to_response(
        'schedule/form.html',
        context,
        RequestContext(request)
    )

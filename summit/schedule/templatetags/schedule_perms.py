# The Summit Scheduler web application
# Copyright (C) 2008 - 2013 Ubuntu Community, Canonical Ltd
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django import template
from django.template import Node, NodeList, Template, Context, Variable

register = template.Library()

@register.tag
def ifchangeschedule(parser, token):
    """
        {% ifchangeschedule summit attendee %}
            scheduler data
        {% else %}
            Restricted
        {% endifchangeschedule %}

    In the above, if user has ``perm`` for ``node``, the node will be displayed, 
    otherwise ``Restricted`` will be displayed
    """
    bits = token.contents.split()
    del bits[0]
    if not len(bits) == 2:
        raise TemplateSyntaxError("'ifchangeschedule' statement requires exactly two argument")
    summit = bits[0]
    attendee = bits[1]
    
    nodelist_true = parser.parse(('else', 'endifchangeschedule'))
    token = parser.next_token()
    if token.contents == 'else':
        nodelist_false = parser.parse(('endifchangeschedule',))
        parser.delete_first_token()
    else:
        nodelist_false = NodeList()
    return IfChangeSchedNode(summit, attendee, nodelist_true, nodelist_false)

class IfChangeSchedNode(Node):
    def __init__(self, summit, attendee, nodelist_true, nodelist_false):
        self.summit = summit
        self.attendee = attendee
        self.nodelist_true, self.nodelist_false = nodelist_true, nodelist_false

    def __repr__(self):
        return "<IfChangeSchedule node>"

    def __iter__(self):
        for node in self.nodelist_true:
            yield node
        for node in self.nodelist_false:
            yield node

    def get_nodes_by_type(self, nodetype):
        nodes = []
        if isinstance(self, nodetype):
            nodes.append(self)
        nodes.extend(self.nodelist_true.get_nodes_by_type(nodetype))
        nodes.extend(self.nodelist_false.get_nodes_by_type(nodetype))
        return nodes

    def render(self, context):
        summit = context.get(self.summit, self.summit)
        attendee = context.get(self.attendee, None)

        if attendee is not None and summit.can_change_agenda(attendee):
            return self.nodelist_true.render(context)
        else:
            return self.nodelist_false.render(context)


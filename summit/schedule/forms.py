# The Summit Scheduler web application
# Copyright (C) 2008 - 2013 Ubuntu Community, Canonical Ltd
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.conf import settings
from django import forms

from summit.schedule.models.meetingmodel import Meeting
from summit.schedule.models.attendeemodel import Attendee
from summit.schedule.models.participantmodel import Participant

from common.forms import RenderableMixin
from common.widgets import DateTimeWidget


class MultipleAttendeeField(forms.ModelMultipleChoiceField):
    def label_from_instance(self, obj):
        return u"%s %s (%s)" % (
            obj.user.first_name,
            obj.user.last_name,
            obj.user.username
        )


class MeetingFormBase(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        if 'instance' in kwargs:
            if kwargs['instance'].pk is not None:
                # We get the 'initial' keyword argument or initialize it
                # as a dict if it didn't exist.
                initial = kwargs.setdefault('initial', {})
                # The widget for a ModelMultipleChoiceField expects
                # a list of primary key for the selected data.
                initial['participants'] = [
                    attendee.pk
                    for attendee in kwargs['instance'].participants.all()]
                initial['speakers'] = [
                    attendee.pk
                    for attendee in kwargs['instance'].speakers.all()]

        super(MeetingFormBase, self).__init__(*args, **kwargs)

        summit = self.instance.summit
        self.fields['eventtype'].queryset = summit.eventtype_set.filter(selectable=True)
        self.fields['tracks'].queryset = summit.track_set.all()

    def save(self, commit=True):
        instance = super(MeetingFormBase, self).save(commit)

        # Prepare a 'save_m2m' method for the form,
        if 'save_m2m' in dir(self):
            old_save_m2m = self.save_m2m
        else:
            old_save_m2m = None

        def save_m2m():
            if old_save_m2m is not None:
                old_save_m2m()
            # This is where we actually link the pizza with toppings
        self.save_m2m = save_m2m

        # Do we need to save all changes now?
        if commit:
            instance.save()
            self.save_m2m()

        return instance


class CreateMeeting(MeetingFormBase, RenderableMixin):
    def __init__(self, *args, **kwargs):
        super(CreateMeeting, self).__init__(*args, **kwargs)
        summit = self.instance.summit
        self.fields['eventtype'].queryset = summit.eventtype_set.all()
        self.fields['speakers'].queryset = Attendee.objects.filter(
                summit=summit
            ).order_by(
                'user__first_name',
                'user__last_name',
                'user__username'
            )

    class Meta:
        model = Meeting
        fields = (
            'title',
            'description',
	    'notes',
	    'speakers',
            'tracks',
            'eventtype',
            'private',
            'urls',
            'duration',
            'slots',
            'approved',
            'video',
            'override_break'
        )


class OrganizerEditMeeting(MeetingFormBase, RenderableMixin):
    def __init__(self, *args, **kwargs):
        super(OrganizerEditMeeting, self).__init__(*args, **kwargs)
        summit = self.instance.summit
        self.fields['eventtype'].queryset = summit.eventtype_set.all()
        self.fields['speakers'].queryset = Attendee.objects.filter(
                summit=summit
            ).order_by(
                'user__first_name',
                'user__last_name',
                'user__username'
            )

    class Meta:
        model = Meeting
        fields = (
            'title',
            'description',
	    'notes',
	    'speakers',
            'tracks',
            'eventtype',
            'private',
            'urls',
            'duration',
            'slots',
            'approved',
            'video',
            'override_break'
        )


class ProposeMeeting(MeetingFormBase, RenderableMixin):
    def __init__(self, *args, **kwargs):
        super(ProposeMeeting, self).__init__(*args, **kwargs)
        self.fields['duration'].choices = ( ("45m", "45 min"), ("20m", "20 min"))

    class Meta:
        model = Meeting
        fields = (
            'title',
            'description',
            'eventtype',
            'duration',
            'tracks',
            'urls',
            'video',
            'notes',
        )


class EditMeeting(MeetingFormBase, RenderableMixin):
    def __init__(self, *args, **kwargs):
        super(EditMeeting, self).__init__(*args, **kwargs)
        self.fields['duration'].choices = ( ("45m", "45 min"), ("20m", "20 min"))

    class Meta:
        model = Meeting
        fields = (
            'title',
            'description',
            'eventtype',
            'duration',
            'tracks',
            'urls',
            'video',
            'notes',
        )


class MeetingReview(forms.ModelForm, RenderableMixin):
    class Meta:
        model = Meeting
        fields = ('approved',)


class AttendMeeting(forms.ModelForm, RenderableMixin):
    class Meta:
        model = Participant
        fields = ('participation',)

    def __init__(self, *args, **kwargs):
        super(AttendMeeting, self).__init__(*args, **kwargs)
        self.fields['participation'].choices = (
            (u'ATTENDING', u'Attending'),
            (u'INTERESTED', u'Very interested in attending')
        )


class OrganizerChangeAttend(forms.ModelForm, RenderableMixin):
    class Meta:
        model = Participant
        fields = ('participation',)

    def __init__(self, *args, **kwargs):
        super(OrganizerChangeAttend, self).__init__(*args, **kwargs)
        self.fields['participation'].choices = (
            (u'INTERESTED', u'Should Attend'),
            (u'REQUIRED', u'Required to Attend')
        )


import re
YOUTUBE_EMBED_HTML = re.compile(r'\<iframe width="\d+" height="\d+" src="https?:\/\/www.youtube.com\/embed\/(?P<youtube_id>.*)" frameborder="0" allowfullscreen\>\<\/iframe\>')
YOUTUBE_PAGE_URL = re.compile(r'https?:\/\/www.youtube.com\/watch\?v=(?P<youtube_id>[^&]*).*')
YOUTUBE_SHORT_URL = re.compile(r'https?:\/\/youtu.be\/(?P<youtube_id>[^\?]*).*')


class YouTubeEmbedURL(forms.URLField):

    def clean(self, value):
        match_html = YOUTUBE_EMBED_HTML.match(value)
        match_page = YOUTUBE_PAGE_URL.match(value)
        match_short = YOUTUBE_SHORT_URL.match(value)
        if match_html:
            value = 'http://www.youtube.com/embed/' + match_html.group('youtube_id')
        elif match_page:
            value = 'http://www.youtube.com/embed/' + match_page.group('youtube_id')
        elif match_short:
            value = 'http://www.youtube.com/embed/' + match_short.group('youtube_id')
        else:
            pass
        return value


class EditMeetingHangout(forms.ModelForm, RenderableMixin):
    class Meta:
        model = Meeting
        fields = ('hangout_url', 'broadcast_url')

    broadcast_url = YouTubeEmbedURL(label='Broadcast URL')


class Registration(forms.ModelForm, RenderableMixin):
    class Meta:
        model = Attendee
        fields = (
            'start_utc',
            'end_utc',
            'crew',
        )

    def __init__(self, *args, **kargs):
        super(Registration, self).__init__(*args, **kargs)
        self.fields['start_utc'].widget = DateTimeWidget()
        self.fields['end_utc'].widget = DateTimeWidget()

    def clean(self):
        begin = self.cleaned_data.get('start_utc')
        end = self.cleaned_data.get('end_utc')
        if begin and end and begin > end:
            raise forms.ValidationError(
                "Availability can not end before it starts."
            )
        return self.cleaned_data

    def save(self, commit=True):
        instance = super(Registration, self).save(commit)
        instance.from_launchpad = False
        instance.save()

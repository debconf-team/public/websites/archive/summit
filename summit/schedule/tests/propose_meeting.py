# The Summit Scheduler web application
# Copyright (C) 2008 - 2013 Ubuntu Community, Canonical Ltd
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime

from django import test as djangotest
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.test.client import Client

from model_mommy import mommy as factory

from summit.schedule.models import (
    Summit,
    Slot,
    Attendee,
    Meeting,
    Track,
    Room,
)


class ProposeMeetingSummitTestCase(djangotest.TestCase):
    """
    Tests for the propose meeting functions
    """

    c = Client()

    def setUp(self):
        now = datetime.datetime.utcnow()
        one_hour = datetime.timedelta(0, 3600)
        week = datetime.timedelta(days=5)
        self.summit = factory.make_one(
            Summit,
            name='uds-virt',
            virtual_summit=True,
            date_start=now,
            date_end=now + week,
        )

        self.slot = factory.make_one(
            Slot,
            start_utc=now+one_hour,
            end_utc=now+(2*one_hour),
            type='open',
            summit=self.summit
        )
        self.track = factory.make_one(Track, summit=self.summit, title='test')
        self.room = factory.make_one(
            Room,
            summit=self.summit,
            type='open',
            irc_channel='test-channel',
        )

        self.user1 = factory.make_one(
            User,
            username='testuser',
            first_name='Test',
            last_name='User',
            is_active=True,
            is_superuser=False,
        )
        self.user1.set_password('password')
        self.user1.save()

    def tearDown(self):
        self.client.logout()

    def login(self):
        logged_in = self.c.login(
            username='testuser',
            password='password')
        self.assertTrue(logged_in)

    def view_and_save_propose_meeting_form(self):
        """
        Tests that a user, attendee or not, can propose a meeting
        """
        # View the edit meeting hangout form
        rev_args = [self.summit.name, ]
        response = self.c.get(
            reverse(
                'summit.schedule.views.propose_meeting',
                args=rev_args
            )
        )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'schedule/propose_meeting.html')

        # Define data to fill our the form
        title = 'Test Meeting'
        description = "This is a test meeting."
        tracks = [self.track.pk]
        spec_url = "http://www.example.com/spec"
        wiki_url = "http://www.example.com/wiki"
        pad_url = "http://www.example.com/pad"
        csrf_token = '%s' % response.context['csrf_token']

        # Post the form
        post = self.c.post(
            reverse(
                'summit.schedule.views.propose_meeting',
                args=rev_args
            ),
            data={
                'csrfmiddlewaretoken': csrf_token,
                'title': title,
                'description': description,
                'tracks': tracks,
                'spec_url': spec_url,
                'wiki_url': wiki_url,
                'pad_url': pad_url,
            }
        )

        # A successful post should redirect to the meeting page
        response = self.view_meeting_page()
        meeting = Meeting.objects.get(title='Test Meeting')
        redirect_args = [self.summit.name, meeting.id, meeting.name]
        redirect_url = reverse(
            'summit.schedule.views.meeting',
            args=redirect_args
        )
        self.assertEqual(post.status_code, 302)
        self.assertRedirects(
            post,
            redirect_url,
        )
        redirect_response = self.c.get(redirect_url)

        # Verify that the new data appears on the meeting page
        self.assertEqual(redirect_response.status_code, 200)
        self.assertIn("Test Meeting", redirect_response.content)

    def view_meeting_page(self):
        meeting = Meeting.objects.get(title='Test Meeting')
        rev_args = [self.summit.name, meeting.id, meeting.name]
        response = self.c.get(
            reverse(
                'summit.schedule.views.meeting',
                args=rev_args
            )
        )
        self.assertEqual(response.status_code, 200)

        return response

    def test_propose_meeting_no_attendee(self):
        """
        Tests that a user who is not an attendee can propose a meeting
        and that an attendee record is created when the user does.
        """

        self.login()

        self.view_and_save_propose_meeting_form()

    def test_propose_meeting_with_attendee(self):
        """
        Tests that a user who is not an attendee can propose a meeting
        and that an attendee record is created when the user does.
        """

        self.attendee1 = factory.make_one(
            Attendee,
            summit=self.summit,
            user=self.user1,
            start_utc=self.summit.date_start,
            end_utc=self.summit.date_end
        )

        self.login()

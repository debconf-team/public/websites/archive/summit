# The Summit Scheduler web application
# Copyright (C) 2008 - 2013 Ubuntu Community, Canonical Ltd
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import datetime
from django import test as djangotest
from django.core.urlresolvers import reverse
from django.conf import settings
from django.contrib.auth.models import User

from model_mommy import mommy as factory
from summit.schedule.fields import NameField

from summit.schedule.models import (
    Summit,
    Slot,
    Meeting,
    Room,
    Agenda,
    Attendee,
    Track,
    Lead,
    Participant,
)

site_root = getattr(settings, 'SITE_ROOT', 'http://summit.ubuntu.com')

# Monkey-patch our NameField into the types of fields that the factory
# understands.  This is simpler than trying to subclass the Mommy
# class directly.
factory.default_mapping[NameField] = str


class MeetingPageTestCase(djangotest.TestCase):

    def test_meeting_page_url(self):
        """ Tests the creation and reverse lookup of meeting page urls"""
        now = datetime.datetime.utcnow()
        one_hour = datetime.timedelta(0, 3600)
        summit = factory.make_one(Summit, name='uds-test')
        summit.save()
        slot = factory.make_one(
            Slot,
            start_utc=now,
            end_utc=now+one_hour,
            type='open',
            summit=summit
        )
        slot.save()

        room = factory.make_one(Room, summit=summit)
        meeting = factory.make_one(
            Meeting,
            summit=summit,
            name='test-meeting',
            private=False
        )
        agenda = factory.make_one(
            Agenda,
            slot=slot,
            meeting=meeting,
            room=room
        )

        # check meeting page url generation
        self.assertEquals(
            meeting.meeting_page_url,
            '/uds-test/meeting/%s/test-meeting/' % meeting.id
        )

        # check meeting page url reverse lookup
        rev_args = ['uds-test', meeting.id, 'test-meeting']
        reverse_url = reverse('summit.schedule.views.meeting', args=rev_args)
        self.assertEquals(
            reverse_url,
            '/uds-test/meeting/%s/test-meeting/' % meeting.id
        )

        # check meeting details page
        response = self.client.get(reverse_url)
        self.assertEquals(response.status_code, 200)

        # check meeting in ical
        response = self.client.get('/uds-test.ical')
        self.assertEquals(response.status_code, 200)
        self.assertContains(
            response,
            'URL:%s/uds-test/meeting/%s/test-meeting/' % (
                site_root,
                meeting.id
            ),
            1
        )

    def test_edit_link_for_both_drafter_and_organizer(self):
        """ Tests that the edit meeting link is present when the user is both drafter and organizer """
        now = datetime.datetime.utcnow()
        one_hour = datetime.timedelta(0, 3600)
        summit = factory.make_one(Summit, name='uds-test', virtual_summit=True)
        summit.save()
        slot = factory.make_one(
            Slot,
            start_utc=now,
            end_utc=now+one_hour,
            type='open',
            summit=summit
        )
        slot.save()

        room = factory.make_one(Room, summit=summit)

        meeting = factory.make_one(
            Meeting,
            summit=summit,
            name='test-meeting',
            private=False
        )
        agenda = factory.make_one(
            Agenda,
            slot=slot,
            meeting=meeting,
            room=room
        )

        user = factory.make_one(
            User,
            username='testuser',
            first_name='Test',
            last_name='User',
            is_active=True,
            is_staff=False,
            is_superuser=False,
        )
        user.set_password('password')
        user.save()
        attendee = factory.make_one(
            Attendee,
            summit=summit,
            user=user,
            start_utc=now,
            end_utc=now+one_hour
        )
        meeting.drafter = attendee
        meeting.save()
        track = factory.make_one(
            Track,
            summit=summit,
        )
        lead = factory.make_one(
            Lead,
            summit=summit,
            track=track,
            lead=attendee,
        )
        

        logged_in = self.client.login(username='testuser', password='password')
        self.assertTrue(logged_in)

        # check meeting page url generation
        self.assertEquals(
            meeting.meeting_page_url,
            '/uds-test/meeting/%s/test-meeting/' % meeting.id
        )

        # check meeting page url reverse lookup
        rev_args = ['uds-test', meeting.id, 'test-meeting']
        reverse_url = reverse('summit.schedule.views.meeting', args=rev_args)
        self.assertEquals(
            reverse_url,
            '/uds-test/meeting/%s/test-meeting/' % meeting.id
        )

        # check meeting details page
        response = self.client.get(reverse_url)
        self.assertEquals(response.status_code, 200)

        self.assertContains(
            response,
            'Edit meeting',
            1
        )

    def test_join_hangout_link_visible_if_attending(self):
        """ Tests that the link to join a hangout is present if the user is attending the summit """
        now = datetime.datetime.utcnow()
        one_hour = datetime.timedelta(0, 3600)
        summit = factory.make_one(Summit, name='uds-test', virtual_summit=True)
        summit.save()
        slot = factory.make_one(
            Slot,
            start_utc=now,
            end_utc=now+one_hour,
            type='open',
            summit=summit
        )
        slot.save()

        room = factory.make_one(Room, summit=summit)

        meeting = factory.make_one(
            Meeting,
            summit=summit,
            name='test-meeting',
            private=False,
            hangout_url='http://test.com/test'
        )
        agenda = factory.make_one(
            Agenda,
            slot=slot,
            meeting=meeting,
            room=room
        )

        user = factory.make_one(
            User,
            username='testuser',
            first_name='Test',
            last_name='User',
            is_active=True,
            is_staff=False,
            is_superuser=False,
        )
        user.set_password('password')
        user.save()
        
        # get meeting page url
        rev_args = ['uds-test', meeting.id, 'test-meeting']
        reverse_url = reverse('summit.schedule.views.meeting', args=rev_args)

        # check before logging in
        response = self.client.get(reverse_url)
        self.assertEquals(response.status_code, 200)
        self.assertContains(
            response,
            '<a href="%s">Join the Hangout on Air</a>' % meeting.hangout_url,
            0
        )

        logged_in = self.client.login(username='testuser', password='password')
        self.assertTrue(logged_in)

        # check after logging in, but not attending
        response = self.client.get(reverse_url)
        self.assertEquals(response.status_code, 200)
        self.assertContains(
            response,
            '<a href="%s">Join the Hangout on Air</a>' % meeting.hangout_url,
            0
        )

        attendee = factory.make_one(
            Attendee,
            summit=summit,
            user=user,
            start_utc=now,
            end_utc=now+one_hour
        )        

        # check after logging in and attending
        response = self.client.get(reverse_url)
        self.assertEquals(response.status_code, 200)
        self.assertContains(
            response,
            '<a href="%s">Join the Hangout on Air</a>' % meeting.hangout_url,
            1
        )

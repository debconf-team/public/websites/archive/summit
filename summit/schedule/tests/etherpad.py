# The Summit Scheduler web application
# Copyright (C) 2008 - 2013 Ubuntu Community, Canonical Ltd
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from django import test as djangotest

from model_mommy import mommy as factory
from summit.schedule.fields import NameField

from summit.schedule.models import (
    Summit,
    Slot,
    Meeting,
    Room,
    Agenda,
)

# Monkey-patch our NameField into the types of fields that the factory
# understands.  This is simpler than trying to subclass the Mommy
# class directly.
factory.default_mapping[NameField] = str


class EtherpadEditUrl(djangotest.TestCase):

    def setUp(self):
        self.summit = factory.make_one(Summit, name='uds-test')
        self.summit.save()

    def tearDown(self):
        pass

    def test_etherpad_edit_url(self):

        slot = factory.make_one(
            Slot,
            type='open',
            summit=self.summit)
        slot.save()

        room = factory.make_one(Room, summit=self.summit)
        meeting = factory.make_one(
            Meeting,
            summit=self.summit,
            name='test-meeting',
            private=False
        )
        agenda = factory.make_one(
            Agenda,
            slot=slot,
            meeting=meeting,
            room=room
        )

        response = self.client.get(
            '/uds-test/meeting/%s/test-meeting/' % meeting.id
        )
        self.assertEquals(response.status_code, 200)
        self.assertContains(
            response,
            'http://pad.ubuntu.com/ep/pad/view/uds-test-test-meeting/latest',
            1
        )

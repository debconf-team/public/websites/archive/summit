# The Summit Scheduler web application
# Copyright (C) 2008 - 2013 Ubuntu Community, Canonical Ltd
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import re
import urllib2
from urlparse import urlparse
from datetime import datetime

from django.conf import settings
from django.db import models
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse

import simplejson as json
from summit.schedule.fields import NameField

from summit.schedule.models.summitmodel import Summit
from summit.schedule.models.eventtypemodel import EventType
from summit.schedule.models.trackmodel import Track
from summit.schedule.models.attendeemodel import Attendee

from summit.schedule.autoslug import AutoSlugMixin

__all__ = (
    'Meeting',
)


class Meeting(AutoSlugMixin, models.Model):

    class SchedulingError(Exception):
        pass

    TYPE_CHOICES = (
        (u'discussion', u'Discussion'),
        (u'boardroom', u'Boardroom'),
        (u'plenary', u'Plenary'),
        (u'roundtable', u'Roundtable'),
        (u'workshop', u'Workshop'),
        (u'presentation', u'Presentation'),
        (u'panel', u'Panel Discussion'),
        (u'seminar', u'Seminar'),
        (u'special', u'Special Event'),
        (u'bof', u'BoF'),
    )

    TYPE_ICONS = {
        u'discussion': None,
        u'boardroom': None,
        u'plenary': None,
        u'roundtable': 'meeting_emblem_roundtable.png',
        u'workshop': 'meeting_emblem_workshop.png',
        u'presentation': 'meeting_emblem_presentation.png',
        u'panel': 'meeting_emblem_panel.png',
        u'seminar': None,
        u'special': None,
        u'bof': 'meeting_emblem_bof.png',
    }

    STATUS_CHOICES = (
        (u'NEW', u'New'),
        (u'DISCUSSION', u'Discussion'),
        (u'DRAFT', u'Drafting'),
        (u'PENDINGREVIEW', u'Review'),
        (u'PENDINGAPPROVAL', u'Pending Approval'),
        (u'APPROVED', u'Approved'),
        (u'SUPERSEDED', u'Superseded'),
        (u'OBSOLETE', u'Obsolete'),
    )

    PRIORITY_CHOICES = (
        (5, u'Undefined'),
        (10, u'Low'),
        (50, u'Medium'),
        (70, u'High'),
        (90, u'Essential'),
    )

    REVIEW_CHOICES = (
        (u'PENDING', u'Pending'),
        (u'APPROVED', u'Approved'),
        (u'DECLINED', u'Declined'),
        (u'REMOVED', u'Removed'),
    )

    DURATION_CHOICES = (
        (u"45m", u"45 min"),
        (u"20m", "20 min"),
        (u"xx", "other"),
    )

    summit = models.ForeignKey(Summit)
    name = NameField(
        max_length=100,
        help_text="Lowercase alphanumeric characters and dashes only."
    )
    title = models.CharField(
        max_length=100,
        help_text="Alphanumeric characters and spaces are allowed"
    )
    description = models.TextField(max_length=2047)
    notes = models.TextField(
	max_length=2047,
	verbose_name="Notes for Content Team (Optional; any private information you want to pass to the content team: scheduling, co-presenters, etc)",
        null=True,
        blank=True
    )
    eventtype = models.ForeignKey(EventType,
        verbose_name="Event type, you most likely want to submit a Talk or a BoF")
    type = models.CharField(
        max_length=15,
        choices=TYPE_CHOICES,
        default=TYPE_CHOICES[0][0]
    )
    status = models.CharField(
        max_length=50,
        choices=STATUS_CHOICES,
        null=True,
        blank=True
    )
    priority = models.IntegerField(
        choices=PRIORITY_CHOICES,
        null=True,
        blank=True
    )
    hangout_url = models.URLField(
        blank=True,
        null=True,
        help_text="URL for participation essential users to join the hangout",
        verify_exists=False,
    )
    broadcast_url = models.URLField(
        blank=True,
        null=True,
        help_text="URL for all other participants to watch the hangout",
        verify_exists=False,
    )
    # FIXME tracks must be for the same summit
    # (will require js magic in admin to refresh the boxes)
    tracks = models.ManyToManyField(
        Track,
        verbose_name=getattr(settings, 'TRACK_DISPLAY_NAME', 'Track'),
        blank=True
    )
    spec_url = models.URLField(
        blank=True,
        verbose_name="Spec URL"
    )
    wiki_url = models.URLField(
        blank=True,
        verify_exists=False,
        verbose_name="Wiki URL"
    )
    pad_url = models.URLField(
        verify_exists=False,
        verbose_name="Pad URL",
        null=True,
        blank=True
    )
    urls = models.TextField(
        blank=True,
        verbose_name="URLs that give extra information about your event"
    )
    # set by video team
    video_url = models.URLField(
        blank=True,
        null=True,
        help_text="URL of (non-life) video",
        verify_exists=False,
    )
    # set by video team / content team [from git annex]
    slides_url = models.URLField(
        blank=True,
        null=True,
        help_text="URL of final slides",
        verify_exists=False,
    )
    duration = models.CharField(verbose_name="Duration of the event",
        max_length=10, default="45 min", blank=True,
        choices=DURATION_CHOICES)
    slots = models.IntegerField(default=1)
    override_break = models.BooleanField(
        default=False,
        verbose_name="Override Break",
        help_text="If this is a multi-slot meeting, should it be"
        "allowed to take place during a break"
    )
    virtual_meeting = models.BooleanField(
        help_text="If virtual summit is true, this will not override it"
    )
    approved = models.CharField(
        max_length=10,
        choices=REVIEW_CHOICES,
        null=True,
        default='PENDING'
    )
    private = models.BooleanField(default=False)
    private_key = models.CharField(max_length=32, null=True, blank=True)

    requires_dial_in = models.BooleanField(
        verbose_name="Dial in",
        help_text="This session requires dial in availability"
    )
    video = models.BooleanField(
        verbose_name="Video recording desired/permitted",
        help_text="This session could be videotaped",
        default=True
    )

    # FIXME attendees must be for the same summit
    # (will require js magic in admin to refresh the boxes)
    drafter = models.ForeignKey(
        Attendee,
        verbose_name="Submitter",
        null=True,
        blank=True,
        related_name='drafter_set'
    )
    assignee = models.ForeignKey(
        Attendee,
        null=True,
        blank=True,
        related_name='assignee_set'
    )
    approver = models.ForeignKey(
        Attendee,
        null=True,
        blank=True,
        related_name='approver_set'
    )
    scribe = models.ForeignKey(
        Attendee,
        null=True,
        blank=True,
        related_name='scribe_set'
    )
    speakers = models.ManyToManyField(
        Attendee,
	verbose_name="Speakers/Coordinators/Moderators",
        blank=True,
	related_name='speaker_set'
    )
    participants = models.ManyToManyField(
        Attendee,
        blank=True,
        through='Participant'
    )
    launchpad_blueprint_id = models.IntegerField(blank=True, null=True)

    class Meta:
        app_label = 'schedule'

    def save(self, *args, **kwargs):
        super(Meeting, self).save(*args, **kwargs)
        self.update_slug()

    def get_decription(self):
        return self.description or ''

    def share(self):
        if not self.pk or not self.private:
            return

        if not self.private_key:
            import hashlib
            import random
            meeting_hash = hashlib.md5()
            meeting_hash.update(str(self.pk))
            meeting_hash.update(self.name)
            meeting_hash.update(str(random.random()))
            self.private_key = meeting_hash.hexdigest()
            self.save()
        return self.private_key

    def _etherpadise_string(self, string):
        return re.sub(r"[^a-zA-Z0-9\-]", '-', string)

    def get_link_to_pad(self):
        if self.pad_url is not None and self.pad_url != '':
            return self.pad_url
        elif self.private:
            import hashlib
            import random
            meeting_hash = hashlib.md5()
            meeting_hash.update(str(self.pk))
            meeting_hash.update(self.name)
            meeting_hash.update(str(random.random()))
            pad_host = self.summit.etherpad
            self.pad_url = '%(host)s%(summit)s-%(meeting)s' % {
                'host': pad_host,
                'summit': self.summit.name,
                'meeting': meeting_hash.hexdigest(),
            }
            self.save()
            return self.pad_url
        else:
            pad_host = self.summit.etherpad
            name = self._etherpadise_string(self.name)
            return '%(host)s%(summit)s-%(meeting)s' % {
                'host': pad_host,
                'summit': self.summit.name,
                'meeting': name,
            }
    link_to_pad = property(get_link_to_pad)

    def get_edit_link_to_pad(self):
        if self.pad_url is not None and self.pad_url != '':
            return self.pad_url
        else:
            pad_host = self.summit.etherpad
            name = self._etherpadise_string(self.name)
            return '%(host)sep/pad/view/%(summit)s-%(meeting)s/latest' % {
                'host': pad_host,
                'summit': self.summit.name,
                'meeting': name,
            }
    edit_link_to_pad = property(get_edit_link_to_pad)

    def get_meeting_page_url(self):
        if self.name is None or self.name == '':
            name = '-'
        else:
            name = self.name
        args = [self.summit.name, self.id, name]
        return reverse('summit.schedule.views.meeting', args=args)
    meeting_page_url = property(get_meeting_page_url)

    def get_meeting_type_icon(self):
        if self.type in self.TYPE_ICONS:
            return self.TYPE_ICONS[self.type]
    icon = property(get_meeting_type_icon)

    def __unicode__(self):
        if self.name:
            return self.name
        else:
            return self.title

    @property
    def track_color(self):
        if self.tracks.exclude(color='FFFFFF').count() > 0:
            return '#'+self.tracks.exclude(color='FFFFFF')[0].color
        else:
            return '#FFFFFF'

    @property
    def spec_priority(self):
        label = dict(self.PRIORITY_CHOICES)[self.priority]
        icon = label.lower()

    def can_edit_pm(self, attendee):
        if attendee is not None:
            if attendee.user == self.approver or attendee.user == self.drafter or attendee.user == self.assignee or attendee in self.speakers or self.summit.is_scheduler(attendee):
                return True
            elif self.summit.managers.filter(pk=attendee.user.pk):
                return True
            elif self.summit.is_tracklead(attendee) and self.summit.lead_set.filter(lead=attendee, track__in=self.tracks.all()).exists():
                return True
            else:
                return False
        else:
            return False

    def can_view_pm(self, attendee):
        if attendee is not None:
            if attendee in self.attendees or attendee in self.speakers:
                return True
            elif attendee.user == self.approver or self.summit.is_scheduler(attendee):
                return True
            elif self.summit.managers.filter(pk=attendee.user.pk):
                return True
            elif self.summit.is_tracklead(attendee) and self.summit.lead_set.filter(lead=attendee, track__in=self.tracks.all()).exists():
                return True
            else:
                return False
        else:
            return False

    def is_tracklead(self, attendee):
        if attendee is not None:
            return self.tracks.filter(lead__lead=attendee).exists()
        return False

    @property
    def attendees(self):
        attendees = []
        if self.scribe and self.scribe not in attendees:
            attendees.append(self.scribe)
        if self.drafter:
            attendees.append(self.drafter)
        if self.assignee and self.assignee not in attendees:
            attendees.append(self.assignee)
#       if self.approver and self.approver not in attendees:
#           attendees.append(self.approver)
        for speaker in self.speakers.all():
	    if speaker not in attendees:
		attendees.append(speaker)

        participants = []
        for attendee in self.participants.all():
            if attendee not in attendees:
                participants.append(attendee)

        attendees.extend(sorted(participants, key=lambda x: x.user.username))
        return attendees

    @property
    def required_attendees(self):
        attendees = set()
        if self.drafter:
            attendees.add(self.drafter)
        if self.assignee:
            attendees.add(self.assignee)
        if self.scribe:
            attendees.add(self.scribe)
        #if self.approver:
        #    attendees.add(self.approver)
        for speaker in self.speakers.all():
            if speaker not in attendees:
                attendees.add(speaker)

        for participant in self.participant_set.all():
            if participant.participation == 'REQUIRED':
                attendees.add(participant.attendee)

        return attendees

    def get_participants_by_level(self, level):
        attendees = set()
        for participant in self.participant_set.all():
            if participant.participation == level:
                attendees.add(participant.attendee)
        return attendees

    def update_from_launchpad(self, elem):
        """Update from Launchpad data."""

        name = elem.get("name")
        if name:
            self.name = name[:100]
        else:
            self.name = ""
        status = elem.get("status")
        if status:
            self.status = status
        else:
            self.status = None

        REMOVED_MEETINGS = ['SUPERSEDED', 'OBSOLETE']
        if self.status in REMOVED_MEETINGS:
            print "Marking %s as removed" % self.name
            self.agenda_set.all().delete()
            self.approved = 'REMOVED'

        if self.status not in REMOVED_MEETINGS and self.approved == 'REMOVED':
            print "Marking %s as APPROVED" % self.name
            self.approved = 'APPROVED'

        if not self.approved == 'REMOVED':
            self.approved = 'APPROVED'

        priority = elem.get("priority")
        if priority:
            self.priority = priority
        else:
            self.priority = None

        spec_url = elem.get("lpurl")
        if spec_url:
            self.spec_url = spec_url

            # fetch to update the title
            # Now, LP has an API, we're stopping the sceen scrape :)
            try:
                apiurl = spec_url.replace(
                    'blueprints.launchpad.net',
                    'api.launchpad.net/devel'
                ) + '?ws.accept=application/json'
                lpdata = urllib2.urlopen(apiurl)
                data = json.load(lpdata)
                if 'title' in data:
                    self.title = data['title'][:100]
                if 'summary' in data:
                    self.description = data['summary']
            except (urllib2.HTTPError, KeyError):
                pass
        else:
            self.spec_url = ''

        wiki_url = elem.get("specurl")
        if wiki_url:
            self.wiki_url = wiki_url
        else:
            self.wiki_url = ''

        # Lookup drafter, assignee and approver before updating.
        # They might not be attending, in which case we don't bother mentioning
        # it (only attendance is interesting to us)
        self.drafter = None
        drafter = elem.get("drafter")
        if drafter:
            self.drafter = self.summit.get_attendee_record(drafter)

        self.assignee = None
        assignee = elem.get("assignee")
        if assignee:
            self.assignee = self.summit.get_attendee_record(assignee)

        self.approver = None
        approver = elem.get("approver")
        if approver:
            self.approver = self.summit.get_attendee_record(approver)

        # Lookup other participants, again ignoring any not attending.
        from_lp = set()
        for p_elem in elem.findall("person"):
            username = p_elem.get("name")
            if not username:
                continue
            from_lp.add(username)

            required = p_elem.get("required", "False") != "False"

            attendee = self.summit.get_attendee_record(username)
            if attendee is not None:
                try:
                    participant = self.participant_set.get(
                        attendee=attendee
                    )
                except ObjectDoesNotExist:
                    participant = self.participant_set.create(
                        attendee=attendee
                    )
                except:
                    participant = self.participant_set.filter(
                        attendee=attendee
                    )[0]

                participant.from_launchpad = True
                if not participant.participation == 'REQUIRED':
                    if required:
                        participant.participation = 'INTERESTED'
                    else:
                        participant.participation = 'ATTENDING'
                participant.save()

        self.participant_set.filter(
            from_launchpad=True
        ).filter(
            ~models.Q(attendee__user__username__in=from_lp)
        ).delete()

        self.save()

    def check_schedule(self, slot, room, with_interested=False):
        """Check whether we can schedule this meeting in the given slot
        and room."""
        missing = set()

        slots = [slot]
        if self.slots > 1:
            slots.extend(s for s
                         in self.summit.slot_set.filter(
                             start_utc__gte=slot.end_utc).order_by('start_utc')
                         if s.start.date() == slot.start.date())
        if len(slots) < self.slots:
            raise Meeting.SchedulingError("Insufficient slots available")

        all_slots = [s for s in self.summit.slot_set.all()
                     if s.start.date() == slot.start.date()]

        slots = slots[:self.slots]
        for this_slot in slots:
            if this_slot.type == 'break' and self.override_break:
                continue
            if this_slot.type not in ('open', 'plenary'):
                raise Meeting.SchedulingError("Slot not available")
            if this_slot.type == 'plenary' and self.type not in (
                'plenary',
                'talk',
                'special'
            ):
                raise Meeting.SchedulingError(
                    "Not a plenary event, talk or special event"
                )

            # Check that the room is not already in use
            try:
                existing = room.agenda_set.get(slot=this_slot)
                if existing.meeting != self:
                    raise Meeting.SchedulingError(
                        "Room is in use by %s"
                        % existing.meeting
                    )
            except ObjectDoesNotExist:
                pass

            # Check that the room is not in use by a double session
            slot_num = all_slots.index(this_slot)
            breaks = 0
            for i in range(slot_num, 0, -1):
                distance = 2 + slot_num - i
                prev_slot = all_slots[i - 1]
                if prev_slot.type != this_slot.type:
                    breaks += 1
                    continue

                for agenda in prev_slot.agenda_set.filter(room=room):
                    if agenda.meeting != self \
                            and agenda.meeting.slots >= (distance - breaks):
                        raise Meeting.SchedulingError(
                            "Room is in use by %s"
                            % agenda.meeting
                        )

            # Check that the room is available
            if not room.available(this_slot):
                raise Meeting.SchedulingError("Room is not available")

            if self.requires_dial_in:
                if not room.has_dial_in:
                    raise Meeting.SchedulingError(
                        "Room has no dial-in capability"
                    )

            # Work out who is busy in this slot
            busy = set()
            for agenda in this_slot.agenda_set.all():
                if agenda.meeting != self:
                    busy.update(
                        [a.pk for a in agenda.meeting.required_attendees]
                    )
                    if with_interested:
                        busy.update(
                            [a.pk for a in agenda.meeting.get_participants_by_level('INTERESTED')]
                        )
            slot_num = all_slots.index(this_slot)
            for i in range(slot_num, 0, -1):
                distance = 2 + slot_num - i
                prev_slot = all_slots[i - 1]
                if prev_slot.type != this_slot.type:
                    break

                for agenda in prev_slot.agenda_set.all():
                    if agenda.meeting != self \
                            and agenda.meeting.slots >= distance:
                        busy.update(
                            [a.pk for a in agenda.meeting.required_attendees]
                        )
                        if with_interested:
                            busy.update(
                                [a.pk for a in agenda.meeting.get_participants_by_level('INTERESTED')]
                            )
            # Check that all required attendees are free in this slot
            for attendee in self.required_attendees:
                if not attendee.available(this_slot):
                    missing.add(attendee)
                if attendee.pk in busy:
                    missing.add(attendee)
            if with_interested:
                for attendee in self.get_participants_by_level('INTERESTED'):
                    if not attendee.available(this_slot):
                        missing.add(attendee)
                    if attendee.pk in busy:
                        missing.add(attendee)

            # Check that next/previous slots are not from the same track
            def check_not_same_track(other_slot, relative_position):
                type_exceptions = ('presentation', 'plenary', 'talk')
                if other_slot is not None and self.type not in type_exceptions:
                    for agenda in other_slot.agenda_set.filter(
                        room__exact=room
                    ).exclude(
                        meeting__exact=self
                    ).filter(
                        meeting__tracks__in=self.tracks.all()
                    ):
                        if agenda.meeting.tracks.filter(
                            pk__in=self.tracks.all()
                        ).exclude(
                            allow_adjacent_sessions=True
                        ).count() > 0:
                            raise Meeting.SchedulingError(
                                "Same track in the %s slot" % relative_position
                            )

            check_not_same_track(this_slot.previous(), "previous")
            check_not_same_track(this_slot.next(), "next")

        # Check that all required attendees can access this room
        # FIXME

        return missing

    def try_schedule(self, with_interested=False):
        """Try to schedule this meeting in a suitable room."""
        if self.private:
            print "Not scheduling private meeting: %s" % self
            return
        if self.type == 'plenary':
            print "Not scheduling plenary: %s" % self
            return
        open_rooms = self.summit.room_set.filter(type__exact='open')

        tracks = self.tracks.all()
        if tracks:
            rooms = open_rooms.filter(tracks__in=tracks)
            if self.try_schedule_into(rooms, with_interested):
                return

        rooms = open_rooms.filter(tracks__isnull=True)
        if not self.try_schedule_into(rooms):
            print "Gave up scheduling %s" % self

    def try_schedule_into(self, rooms, with_interested=False):
        """Try to schedule this meeting in one of the given rooms."""
        today = datetime.now()
        for room in rooms:
            for slot in self.summit.slot_set.filter(
                type__exact='open',
                start_utc__gt=today
            ):
                try:
                    room.agenda_set.get(slot=slot)
                except room.agenda_set.model.DoesNotExist:
                    try:
                        missing = self.check_schedule(
                            slot,
                            room,
                            with_interested
                        )
                        if len(missing):
                            raise Meeting.SchedulingError(
                                "Required people not available: %s"
                                % ', '.join(m.user.username for m in missing)
                            )

                        print "Schedule %s in %s at %s" % (self, room, slot)
                        room.agenda_set.create(
                            slot=slot,
                            meeting=self,
                            auto=True
                        )
                        return True
                    except Meeting.SchedulingError, e:
                        print "-- could not schedule %s in %s at %s (%s)" % (
                            self,
                            room,
                            slot,
                            e
                        )

        return False

    @property
    def irc_logs(self):
        logs = list()
        for agenda in self.agenda_set.all():
            logs.append(agenda.irc_log)
        return logs
        
        
    @property
    def current(self):
        for agenda in self.agenda_set.all():
            if agenda.slot.current:
                return True
        return False

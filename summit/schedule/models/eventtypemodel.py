# The Summit Scheduler web application
# Copyright (C) 2008 - 2013 Ubuntu Community, Canonical Ltd
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.db import models

from summit.schedule.models.summitmodel import Summit
from summit.common.utils import hex_to_rgba

__all__ = (
    'EventType',
)


class EventType(models.Model):
    summit = models.ForeignKey(Summit)
    selectable = models.BooleanField(default=True, verbose_name="Attendee can create event this category")
    name = models.CharField(max_length=16)
    plural = models.CharField(max_length=16)
    icon = models.CharField(max_length=256, blank=True)
    color = models.CharField(
        max_length=6,
        null=False,
        blank=False,
        default='FFFFFF'
    )
    note = models.TextField(max_length=1000, blank=True, null=True)

    class Meta:
        app_label = 'schedule'
        ordering = ('name', 'summit')

    def __unicode__(self):
        return "%s" % (self.name)

    def get_color(self):
        if self.color:
            return "#%s" % self.color
        else:
            return "#ffffff"

    def get_bordercolor(self):
        return "1px solid %s" % self.get_color()

    def get_bgcolor(self):
        return "rgba%s" % str(hex_to_rgba(self.get_color()))


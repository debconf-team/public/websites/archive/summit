import os
import sys

from settings import *

SITE_ROOT = 'https://summit.debconf.org'

SITE_ID = 1

ROOT_URLCONF = 'summit.debconf_website.urls'
LOGIN_URL = '/login/debian-oauth2'
STATIC_URL = '/static/debconf_website/'

try:
    import debconf_website

    INSTALLED_APPS.append('debconf_website')
    INSTALLED_APPS.append('volunteers')

    TEMPLATE_CONTEXT_PROCESSORS += (
        "debconf_website.media_processor",
        "debconf_website.popup_check",
    )
    TEMPLATE_DIRS += (
        debconf_website.TEMPLATE_DIR,
    )

    THEME_MEDIA = debconf_website.MEDIA_ROOT
except ImportError:
    if not 'init-summit' in sys.argv:
        print "You will need to run ./manage.py init-summit to make The Summit Scheduler fully work."
    else:
        pass

# Days before the start of the summit when we stop allowing
# track leads to edit the schedule
TRACK_LEAD_SCHEDULE_BLACKOUT = 0
WEBCHAT_URL = 'https://kiwiirc.com/client/irc.freenode.com/%(channel_name)s'

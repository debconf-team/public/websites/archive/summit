======================
Track Lead Permissions
======================

**Managing Meeting Proposals**

The track leads are responsible for accepting or rejecting proposals. As a track lead, you need to visit the list of blueprints for the upcoming summit to approve or decline blueprints that have been proposed to meetings.

You should see a sentence before the footer like:

You can show topics that have been declined and since you are a driver of Linaro Connect Q4.11 you can also accept meetings or topics that have been proposed.
All the track leads should have this authority. If you don't, you'll need to speak to the correct person in your organization.

The accept/reject interface is self-explanatory.

**Tips**

If you like a proposal but want some updates to it, don't reject it. Its easier to tell the person to make the updates and then check back and accept it.

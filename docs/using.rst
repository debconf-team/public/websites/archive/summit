==========================
Using The Summit Scheduler
==========================

The Summit Scheduler is a web tool utilized at Ubuntu Developer Summit and Linaro Connect.

Registering to Attend a Summit
==============================
In order to use Summit for your personal schedule, you will need to register in Launchpad as attending the summit. To do this, click the "Register in Launchpad" link on the main page for the summit.

*Note*: Once you register, it may take up to 20 minutes for you to be able to use Summit.


Signing up to Attend a Meeting
==============================
After you have registered yourself as attending a Summit, you have the ability to "attend meetings." Signing yourself up to attend meetings will create your own personal schedule.

To sign up to attend a meeting you have 2 options:

**Subscribe to the blueprint**

If a meeting has a blueprint, you can subscribe yourself to the blueprint which will mark you as attending the meeting. Subscribing to a blueprint will also notify you each time that the blueprint is changed. Sign up this way if you are interested in following the progress of the topic after the summit is over.

When subscribing to the blueprint there is the ability to mark yourself as "Participation essential." By doing this, the Summit Scheduler will attempt to make the meeting at a time in which you are available. 

*Note: Please only subscribe to a blueprint as Participation essential if your participation truely is essential.*

**Subscribe to the meeting in Summit**

If you do not want to follow the progress of the topic after the summit is over, or if the meeting does not have a blueprint,ou will want to subscribe to the meeting in Summit. Signing up this way will still add the meeting to your personal calendar, however you will not get emails after the summit when the blueprint changes.

To subscribe to a meeting in Summit, click on the meeting name and you will be sent to the meeting page. In the navigation near the top, you will see a link called ""Attend this meeting." Shortly after clicking this link, the meeting will show up in your personal schedule.

Agenda View
===========

When you are logged into Summit, the agenda view will show your personal schedule. All of the meetings have a star infront of the name of the meeting. If this star is grey you are not subscribed to the meeting. If the star is gold, you are subscribed.

If you mouseover the meeting name, you will see the details about the meeting. This includes the meeting description, participants, and tracks. Any participants with a red star next to their name are "Participation essential."

Wide Screen View
================

The Wide Screen View is the traditional schedule view. This view is no longer recommended for use on personal computers.

The Wide Screen View is the primary view for the monitors around the halls of the summit for reference when moving between meetings.

Track View
==========

The Track View will show you all of the meetings for a particular track.

Room View
=========

The Room View will show you all of the meetings for a particular room.

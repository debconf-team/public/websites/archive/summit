==================
Running a Summit
==================

----------------
Create a Summit
----------------

In the Summit Scheduler
-----------------------

**Name**

This field will become the URL of the summit. May only contain lowercase letters, numbers and hyphens.

**Title**

The display name of your summit. May contain uppercase and lowercase letters and numbers.

**Description**

A description of the event. You may use markdown styling.

**Etherpad**

The Summit Scheduler uses etherpad for taking notes during meetings. Please provide the base URL for the etherpad instance you would like to use.

**QR**

If your summit has a mobile application to display the schedule, you may enter the URL to the QR image here.

**Hashtag**

This is useful if you wish to display a twitter feed of a certain hashtag on the Summit page.

**State**

 * Sponsorship Requests  -  During this state users will be able to apply for sponsorship to the summit event.
 * Sponsorship Reviews  -  This status will turn off the ability for users to request sponsorship to your summit and turn on the ability for a defined group of users to review the sponsorship requests and vote on them.
 * Scheduling  -  This status is what should be used prior to and during the summit. It will give users the ability to propose new meetings and mark themselves as attending meetings.
 * Public  -  This status should be set after the event is over. This will prevent users from being able to create new meetings and remove other features that are used prior to and during a summit.
 
**Help**

This field is used for displaying a message across the bottom of each page. Markdown styling is supported.

**Sites**

The Summit Scheduler is capable of running multiple different sites off of one code base while appearing to be completely seperate sites. This field is what defines which site the summit will appear on. Multiple selections are allowed for co-hosted events.

**Managers**

This field allows you to define users who will have manager permissions. For more information on the permissions given to a manager, view the Summit Scheduler Permissions page.

**Schedulers**

This field allows you to define users who are allowed to have scheduler permissions. For more information on the permissions given to a scheduler, view the Summit Scheduler Permissions page.

**Tracks**

 * Title  -  The display title of your track.
 * Slug  -  The track slug may contain lowercase letters, numbers, and hyphens.
 * Color  -  Adding a color allows for an obvious differentiation between different tracks.
 * Description  -  A brief description of the track. This field supports markdown styling.
 * Allow adjacent sessions  -  By default, the Summit Scheduler does not allow two sessions from the same track to be scheduled in the same room. This is to encourage participants to get up, move around, and have more interaction with other participants. If you would like to allow the Summit Scheduler to have sessions from the same track back to back in the same room, check this box.
 * Delete  -  Check this box if you want to delete an already created track
 
**Sprint**

Add the export URL for the Launchpad sprint that was created for this event.

In Launchpad
------------

------
Rooms
------

**Name**

Name of the room. May contain lowercase letters and numbers.

**Title**

Display title of the room. May contain uppercase and lowercase letters, numbers and spaces.

**Type**

 * Openly Scheduled  -  May contain any meeting other than a plenary.
 * Plenary Events  -  May contain only plenaries.
 * Closed  -  Room that will not be used.
 * Private  -  A private room that is not displayed to people without the correct permissions or attendees to private meetings in the room.

**Size**

The number of people the room can hold.

**Tracks**

You are able to set the room up so that only certain tracks will be scheduled in the room. This is helpful for special setups for things like hacking sessions.

**Icecast URL**

This can be the URL to any type of audio or audio video link. Ex. Icecast, Google Hangouts

**IRC Channel**

The IRC channel name without the hash (#).

**Has Dial In**

If this room has dial in capabilities, meetings that require dial in will be scheduled here.

**Availability**

General starting and ending availability of the room. Must be entered in UTC time.

**Busy**

Used to set certain time periods throughout the sprint in which this room is not available.

------------
Track Leads
------------

-------
Slots
-------

------
Crew
------

----------------
Crons
----------------

**Auto Schedule**

**Reschedule**

**Dead blueprints**

----------------
Track Lead
----------------

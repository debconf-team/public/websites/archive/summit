=======
Credits
=======

This documenation was largely adapted from the `documenation for zamboni
<http://jbalogh.github.com/zamboni>`_ and from `Sharing your development
across branches <http://micknelson.wordpress.com/2011/05/19/sharing-your-development-environment-across-branches/>`_
article from Michael Nelson.
